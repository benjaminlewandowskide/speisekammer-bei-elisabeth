// Avoid `console` errors in browsers that lack a console.
(function () {
  var method;

  var noop = function () {};

  var methods = ['assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error', 'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log', 'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd', 'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn'];
  var length = methods.length;
  var console = window.console = window.console || {};

  while (length--) {
    method = methods[length]; // Only stub undefined methods.

    if (!console[method]) {
      console[method] = noop;
    }
  }
})();
(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() : typeof define === 'function' && define.amd ? define(factory) : (global = global || self, global.GLightbox = factory());
})(this, function () {
  'use strict';

  function _typeof(obj) {
    if (typeof Symbol === 'function' && typeof Symbol.iterator === 'symbol') {
      _typeof = function (obj) {
        return typeof obj;
      };
    } else {
      _typeof = function (obj) {
        return obj && typeof Symbol === 'function' && obj.constructor === Symbol && obj !== Symbol.prototype ? 'symbol' : typeof obj;
      };
    }

    return _typeof(obj);
  }

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError('Cannot call a class as a function');
    }
  }

  function _defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ('value' in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  function _createClass(Constructor, protoProps, staticProps) {
    if (protoProps) _defineProperties(Constructor.prototype, protoProps);
    if (staticProps) _defineProperties(Constructor, staticProps);
    return Constructor;
  }

  function _toConsumableArray(arr) {
    return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread();
  }

  function _arrayWithoutHoles(arr) {
    if (Array.isArray(arr)) {
      for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) arr2[i] = arr[i];

      return arr2;
    }
  }

  function _iterableToArray(iter) {
    if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === '[object Arguments]') return Array.from(iter);
  }

  function _nonIterableSpread() {
    throw new TypeError('Invalid attempt to spread non-iterable instance');
  }

  function getLen(v) {
    return Math.sqrt(v.x * v.x + v.y * v.y);
  }

  function dot(v1, v2) {
    return v1.x * v2.x + v1.y * v2.y;
  }

  function getAngle(v1, v2) {
    var mr = getLen(v1) * getLen(v2);
    if (mr === 0) return 0;
    var r = dot(v1, v2) / mr;
    if (r > 1) r = 1;
    return Math.acos(r);
  }

  function cross(v1, v2) {
    return v1.x * v2.y - v2.x * v1.y;
  }

  function getRotateAngle(v1, v2) {
    var angle = getAngle(v1, v2);

    if (cross(v1, v2) > 0) {
      angle *= -1;
    }

    return angle * 180 / Math.PI;
  }

  var EventsHandlerAdmin = function () {
    function EventsHandlerAdmin(el) {
      _classCallCheck(this, EventsHandlerAdmin);

      this.handlers = [];
      this.el = el;
    }

    _createClass(EventsHandlerAdmin, [{
      key: 'add',
      value: function add(handler) {
        this.handlers.push(handler);
      }
    }, {
      key: 'del',
      value: function del(handler) {
        if (!handler) this.handlers = [];

        for (var i = this.handlers.length; i >= 0; i--) {
          if (this.handlers[i] === handler) {
            this.handlers.splice(i, 1);
          }
        }
      }
    }, {
      key: 'dispatch',
      value: function dispatch() {
        for (var i = 0, len = this.handlers.length; i < len; i++) {
          var handler = this.handlers[i];
          if (typeof handler === 'function') handler.apply(this.el, arguments);
        }
      }
    }]);

    return EventsHandlerAdmin;
  }();

  function wrapFunc(el, handler) {
    var EventshandlerAdmin = new EventsHandlerAdmin(el);
    EventshandlerAdmin.add(handler);
    return EventshandlerAdmin;
  }

  var TouchEvents = function () {
    function TouchEvents(el, option) {
      _classCallCheck(this, TouchEvents);

      this.element = typeof el == 'string' ? document.querySelector(el) : el;
      this.start = this.start.bind(this);
      this.move = this.move.bind(this);
      this.end = this.end.bind(this);
      this.cancel = this.cancel.bind(this);
      this.element.addEventListener('touchstart', this.start, false);
      this.element.addEventListener('touchmove', this.move, false);
      this.element.addEventListener('touchend', this.end, false);
      this.element.addEventListener('touchcancel', this.cancel, false);
      this.preV = {
        x: null,
        y: null
      };
      this.pinchStartLen = null;
      this.zoom = 1;
      this.isDoubleTap = false;

      var noop = function noop() {};

      this.rotate = wrapFunc(this.element, option.rotate || noop);
      this.touchStart = wrapFunc(this.element, option.touchStart || noop);
      this.multipointStart = wrapFunc(this.element, option.multipointStart || noop);
      this.multipointEnd = wrapFunc(this.element, option.multipointEnd || noop);
      this.pinch = wrapFunc(this.element, option.pinch || noop);
      this.swipe = wrapFunc(this.element, option.swipe || noop);
      this.tap = wrapFunc(this.element, option.tap || noop);
      this.doubleTap = wrapFunc(this.element, option.doubleTap || noop);
      this.longTap = wrapFunc(this.element, option.longTap || noop);
      this.singleTap = wrapFunc(this.element, option.singleTap || noop);
      this.pressMove = wrapFunc(this.element, option.pressMove || noop);
      this.twoFingerPressMove = wrapFunc(this.element, option.twoFingerPressMove || noop);
      this.touchMove = wrapFunc(this.element, option.touchMove || noop);
      this.touchEnd = wrapFunc(this.element, option.touchEnd || noop);
      this.touchCancel = wrapFunc(this.element, option.touchCancel || noop);
      this._cancelAllHandler = this.cancelAll.bind(this);
      window.addEventListener('scroll', this._cancelAllHandler);
      this.delta = null;
      this.last = null;
      this.now = null;
      this.tapTimeout = null;
      this.singleTapTimeout = null;
      this.longTapTimeout = null;
      this.swipeTimeout = null;
      this.x1 = this.x2 = this.y1 = this.y2 = null;
      this.preTapPosition = {
        x: null,
        y: null
      };
    }

    _createClass(TouchEvents, [{
      key: 'start',
      value: function start(evt) {
        if (!evt.touches) return;
        this.now = Date.now();
        this.x1 = evt.touches[0].pageX;
        this.y1 = evt.touches[0].pageY;
        this.delta = this.now - (this.last || this.now);
        this.touchStart.dispatch(evt, this.element);

        if (this.preTapPosition.x !== null) {
          this.isDoubleTap = this.delta > 0 && this.delta <= 250 && Math.abs(this.preTapPosition.x - this.x1) < 30 && Math.abs(this.preTapPosition.y - this.y1) < 30;
          if (this.isDoubleTap) clearTimeout(this.singleTapTimeout);
        }

        this.preTapPosition.x = this.x1;
        this.preTapPosition.y = this.y1;
        this.last = this.now;
        var preV = this.preV,
            len = evt.touches.length;

        if (len > 1) {
          this._cancelLongTap();

          this._cancelSingleTap();

          var v = {
            x: evt.touches[1].pageX - this.x1,
            y: evt.touches[1].pageY - this.y1
          };
          preV.x = v.x;
          preV.y = v.y;
          this.pinchStartLen = getLen(preV);
          this.multipointStart.dispatch(evt, this.element);
        }

        this._preventTap = false;
        this.longTapTimeout = setTimeout(function () {
          this.longTap.dispatch(evt, this.element);
          this._preventTap = true;
        }.bind(this), 750);
      }
    }, {
      key: 'move',
      value: function move(evt) {
        if (!evt.touches) return;
        var preV = this.preV,
            len = evt.touches.length,
            currentX = evt.touches[0].pageX,
            currentY = evt.touches[0].pageY;
        this.isDoubleTap = false;

        if (len > 1) {
          var sCurrentX = evt.touches[1].pageX,
              sCurrentY = evt.touches[1].pageY;
          var v = {
            x: evt.touches[1].pageX - currentX,
            y: evt.touches[1].pageY - currentY
          };

          if (preV.x !== null) {
            if (this.pinchStartLen > 0) {
              evt.zoom = getLen(v) / this.pinchStartLen;
              this.pinch.dispatch(evt, this.element);
            }

            evt.angle = getRotateAngle(v, preV);
            this.rotate.dispatch(evt, this.element);
          }

          preV.x = v.x;
          preV.y = v.y;

          if (this.x2 !== null && this.sx2 !== null) {
            evt.deltaX = (currentX - this.x2 + sCurrentX - this.sx2) / 2;
            evt.deltaY = (currentY - this.y2 + sCurrentY - this.sy2) / 2;
          } else {
            evt.deltaX = 0;
            evt.deltaY = 0;
          }

          this.twoFingerPressMove.dispatch(evt, this.element);
          this.sx2 = sCurrentX;
          this.sy2 = sCurrentY;
        } else {
          if (this.x2 !== null) {
            evt.deltaX = currentX - this.x2;
            evt.deltaY = currentY - this.y2;
            var movedX = Math.abs(this.x1 - this.x2),
                movedY = Math.abs(this.y1 - this.y2);

            if (movedX > 10 || movedY > 10) {
              this._preventTap = true;
            }
          } else {
            evt.deltaX = 0;
            evt.deltaY = 0;
          }

          this.pressMove.dispatch(evt, this.element);
        }

        this.touchMove.dispatch(evt, this.element);

        this._cancelLongTap();

        this.x2 = currentX;
        this.y2 = currentY;

        if (len > 1) {
          evt.preventDefault();
        }
      }
    }, {
      key: 'end',
      value: function end(evt) {
        if (!evt.changedTouches) return;

        this._cancelLongTap();

        var self = this;

        if (evt.touches.length < 2) {
          this.multipointEnd.dispatch(evt, this.element);
          this.sx2 = this.sy2 = null;
        }

        if (this.x2 && Math.abs(this.x1 - this.x2) > 30 || this.y2 && Math.abs(this.y1 - this.y2) > 30) {
          evt.direction = this._swipeDirection(this.x1, this.x2, this.y1, this.y2);
          this.swipeTimeout = setTimeout(function () {
            self.swipe.dispatch(evt, self.element);
          }, 0);
        } else {
          this.tapTimeout = setTimeout(function () {
            if (!self._preventTap) {
              self.tap.dispatch(evt, self.element);
            }

            if (self.isDoubleTap) {
              self.doubleTap.dispatch(evt, self.element);
              self.isDoubleTap = false;
            }
          }, 0);

          if (!self.isDoubleTap) {
            self.singleTapTimeout = setTimeout(function () {
              self.singleTap.dispatch(evt, self.element);
            }, 250);
          }
        }

        this.touchEnd.dispatch(evt, this.element);
        this.preV.x = 0;
        this.preV.y = 0;
        this.zoom = 1;
        this.pinchStartLen = null;
        this.x1 = this.x2 = this.y1 = this.y2 = null;
      }
    }, {
      key: 'cancelAll',
      value: function cancelAll() {
        this._preventTap = true;
        clearTimeout(this.singleTapTimeout);
        clearTimeout(this.tapTimeout);
        clearTimeout(this.longTapTimeout);
        clearTimeout(this.swipeTimeout);
      }
    }, {
      key: 'cancel',
      value: function cancel(evt) {
        this.cancelAll();
        this.touchCancel.dispatch(evt, this.element);
      }
    }, {
      key: '_cancelLongTap',
      value: function _cancelLongTap() {
        clearTimeout(this.longTapTimeout);
      }
    }, {
      key: '_cancelSingleTap',
      value: function _cancelSingleTap() {
        clearTimeout(this.singleTapTimeout);
      }
    }, {
      key: '_swipeDirection',
      value: function _swipeDirection(x1, x2, y1, y2) {
        return Math.abs(x1 - x2) >= Math.abs(y1 - y2) ? x1 - x2 > 0 ? 'Left' : 'Right' : y1 - y2 > 0 ? 'Up' : 'Down';
      }
    }, {
      key: 'on',
      value: function on(evt, handler) {
        if (this[evt]) {
          this[evt].add(handler);
        }
      }
    }, {
      key: 'off',
      value: function off(evt, handler) {
        if (this[evt]) {
          this[evt].del(handler);
        }
      }
    }, {
      key: 'destroy',
      value: function destroy() {
        if (this.singleTapTimeout) clearTimeout(this.singleTapTimeout);
        if (this.tapTimeout) clearTimeout(this.tapTimeout);
        if (this.longTapTimeout) clearTimeout(this.longTapTimeout);
        if (this.swipeTimeout) clearTimeout(this.swipeTimeout);
        this.element.removeEventListener('touchstart', this.start);
        this.element.removeEventListener('touchmove', this.move);
        this.element.removeEventListener('touchend', this.end);
        this.element.removeEventListener('touchcancel', this.cancel);
        this.rotate.del();
        this.touchStart.del();
        this.multipointStart.del();
        this.multipointEnd.del();
        this.pinch.del();
        this.swipe.del();
        this.tap.del();
        this.doubleTap.del();
        this.longTap.del();
        this.singleTap.del();
        this.pressMove.del();
        this.twoFingerPressMove.del();
        this.touchMove.del();
        this.touchEnd.del();
        this.touchCancel.del();
        this.preV = this.pinchStartLen = this.zoom = this.isDoubleTap = this.delta = this.last = this.now = this.tapTimeout = this.singleTapTimeout = this.longTapTimeout = this.swipeTimeout = this.x1 = this.x2 = this.y1 = this.y2 = this.preTapPosition = this.rotate = this.touchStart = this.multipointStart = this.multipointEnd = this.pinch = this.swipe = this.tap = this.doubleTap = this.longTap = this.singleTap = this.pressMove = this.touchMove = this.touchEnd = this.touchCancel = this.twoFingerPressMove = null;
        window.removeEventListener('scroll', this._cancelAllHandler);
        return null;
      }
    }]);

    return TouchEvents;
  }();

  var ZoomImages = function () {
    function ZoomImages(el, slide) {
      var _this = this;

      var onclose = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;

      _classCallCheck(this, ZoomImages);

      this.img = el;
      this.slide = slide;
      this.onclose = onclose;

      if (this.img.setZoomEvents) {
        return false;
      }

      this.active = false;
      this.zoomedIn = false;
      this.dragging = false;
      this.currentX = null;
      this.currentY = null;
      this.initialX = null;
      this.initialY = null;
      this.xOffset = 0;
      this.yOffset = 0;
      this.img.addEventListener('mousedown', function (e) {
        return _this.dragStart(e);
      }, false);
      this.img.addEventListener('mouseup', function (e) {
        return _this.dragEnd(e);
      }, false);
      this.img.addEventListener('mousemove', function (e) {
        return _this.drag(e);
      }, false);
      this.img.addEventListener('click', function (e) {
        if (!_this.zoomedIn) {
          return _this.zoomIn();
        }

        if (_this.zoomedIn && !_this.dragging) {
          _this.zoomOut();
        }
      }, false);
      this.img.setZoomEvents = true;
    }

    _createClass(ZoomImages, [{
      key: 'zoomIn',
      value: function zoomIn() {
        var winWidth = this.widowWidth();

        if (this.zoomedIn || winWidth <= 768) {
          return;
        }

        var img = this.img;
        img.setAttribute('data-style', img.getAttribute('style'));
        img.style.maxWidth = img.naturalWidth + 'px';
        img.style.maxHeight = img.naturalHeight + 'px';

        if (img.naturalWidth > winWidth) {
          var centerX = winWidth / 2 - img.naturalWidth / 2;
          this.setTranslate(this.img.parentNode, centerX, 0);
        }

        this.slide.classList.add('zoomed');
        this.zoomedIn = true;
      }
    }, {
      key: 'zoomOut',
      value: function zoomOut() {
        this.img.parentNode.setAttribute('style', '');
        this.img.setAttribute('style', this.img.getAttribute('data-style'));
        this.slide.classList.remove('zoomed');
        this.zoomedIn = false;
        this.currentX = null;
        this.currentY = null;
        this.initialX = null;
        this.initialY = null;
        this.xOffset = 0;
        this.yOffset = 0;

        if (this.onclose && typeof this.onclose == 'function') {
          this.onclose();
        }
      }
    }, {
      key: 'dragStart',
      value: function dragStart(e) {
        e.preventDefault();

        if (!this.zoomedIn) {
          this.active = false;
          return;
        }

        if (e.type === 'touchstart') {
          this.initialX = e.touches[0].clientX - this.xOffset;
          this.initialY = e.touches[0].clientY - this.yOffset;
        } else {
          this.initialX = e.clientX - this.xOffset;
          this.initialY = e.clientY - this.yOffset;
        }

        if (e.target === this.img) {
          this.active = true;
          this.img.classList.add('dragging');
        }
      }
    }, {
      key: 'dragEnd',
      value: function dragEnd(e) {
        var _this2 = this;

        e.preventDefault();
        this.initialX = this.currentX;
        this.initialY = this.currentY;
        this.active = false;
        setTimeout(function () {
          _this2.dragging = false;
          _this2.img.isDragging = false;

          _this2.img.classList.remove('dragging');
        }, 100);
      }
    }, {
      key: 'drag',
      value: function drag(e) {
        if (this.active) {
          e.preventDefault();

          if (e.type === 'touchmove') {
            this.currentX = e.touches[0].clientX - this.initialX;
            this.currentY = e.touches[0].clientY - this.initialY;
          } else {
            this.currentX = e.clientX - this.initialX;
            this.currentY = e.clientY - this.initialY;
          }

          this.xOffset = this.currentX;
          this.yOffset = this.currentY;
          this.img.isDragging = true;
          this.dragging = true;
          this.setTranslate(this.img, this.currentX, this.currentY);
        }
      }
    }, {
      key: 'onMove',
      value: function onMove(e) {
        if (!this.zoomedIn) {
          return;
        }

        var xOffset = e.clientX - this.img.naturalWidth / 2;
        var yOffset = e.clientY - this.img.naturalHeight / 2;
        this.setTranslate(this.img, xOffset, yOffset);
      }
    }, {
      key: 'setTranslate',
      value: function setTranslate(node, xPos, yPos) {
        node.style.transform = 'translate3d(' + xPos + 'px, ' + yPos + 'px, 0)';
      }
    }, {
      key: 'widowWidth',
      value: function widowWidth() {
        return window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
      }
    }]);

    return ZoomImages;
  }();

  var isMobile = navigator.userAgent.match(/(iPad)|(iPhone)|(iPod)|(Android)|(PlayBook)|(BB10)|(BlackBerry)|(Opera Mini)|(IEMobile)|(webOS)|(MeeGo)/i);
  var isTouch = isMobile !== null || document.createTouch !== undefined || 'ontouchstart' in window || 'onmsgesturechange' in window || navigator.msMaxTouchPoints;
  var html = document.getElementsByTagName('html')[0];
  var transitionEnd = whichTransitionEvent();
  var animationEnd = whichAnimationEvent();
  var uid = Date.now();
  var videoPlayers = {};
  var defaults = {
    selector: 'glightbox',
    elements: null,
    skin: 'clean',
    closeButton: true,
    startAt: null,
    autoplayVideos: true,
    descPosition: 'bottom',
    width: 900,
    height: 506,
    videosWidth: 960,
    beforeSlideChange: null,
    afterSlideChange: null,
    beforeSlideLoad: null,
    afterSlideLoad: null,
    onOpen: null,
    onClose: null,
    loop: false,
    touchNavigation: true,
    touchFollowAxis: true,
    keyboardNavigation: true,
    closeOnOutsideClick: true,
    plyr: {
      css: 'https://cdn.plyr.io/3.5.6/plyr.css',
      js: 'https://cdn.plyr.io/3.5.6/plyr.js',
      ratio: '16:9',
      config: {
        youtube: {
          noCookie: true,
          rel: 0,
          showinfo: 0,
          iv_load_policy: 3
        },
        vimeo: {
          byline: false,
          portrait: false,
          title: false,
          transparent: false
        }
      }
    },
    openEffect: 'zoomIn',
    closeEffect: 'zoomOut',
    slideEffect: 'slide',
    moreText: 'See more',
    moreLength: 60,
    lightboxHtml: '',
    cssEfects: {
      fade: {
        in: 'fadeIn',
        out: 'fadeOut'
      },
      zoom: {
        in: 'zoomIn',
        out: 'zoomOut'
      },
      slide: {
        in: 'slideInRight',
        out: 'slideOutLeft'
      },
      slide_back: {
        in: 'slideInLeft',
        out: 'slideOutRight'
      }
    },
    svg: {
      close: '<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" xml:space="preserve"><g><g><path d="M505.943,6.058c-8.077-8.077-21.172-8.077-29.249,0L6.058,476.693c-8.077,8.077-8.077,21.172,0,29.249C10.096,509.982,15.39,512,20.683,512c5.293,0,10.586-2.019,14.625-6.059L505.943,35.306C514.019,27.23,514.019,14.135,505.943,6.058z"/></g></g><g><g><path d="M505.942,476.694L35.306,6.059c-8.076-8.077-21.172-8.077-29.248,0c-8.077,8.076-8.077,21.171,0,29.248l470.636,470.636c4.038,4.039,9.332,6.058,14.625,6.058c5.293,0,10.587-2.019,14.624-6.057C514.018,497.866,514.018,484.771,505.942,476.694z"/></g></g></svg>',
      next: '<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 477.175 477.175" xml:space="preserve"> <g><path d="M360.731,229.075l-225.1-225.1c-5.3-5.3-13.8-5.3-19.1,0s-5.3,13.8,0,19.1l215.5,215.5l-215.5,215.5c-5.3,5.3-5.3,13.8,0,19.1c2.6,2.6,6.1,4,9.5,4c3.4,0,6.9-1.3,9.5-4l225.1-225.1C365.931,242.875,365.931,234.275,360.731,229.075z"/></g></svg>',
      prev: '<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 477.175 477.175" xml:space="preserve"><g><path d="M145.188,238.575l215.5-215.5c5.3-5.3,5.3-13.8,0-19.1s-13.8-5.3-19.1,0l-225.1,225.1c-5.3,5.3-5.3,13.8,0,19.1l225.1,225c2.6,2.6,6.1,4,9.5,4s6.9-1.3,9.5-4c5.3-5.3,5.3-13.8,0-19.1L145.188,238.575z"/></g></svg>'
    }
  };
  var lightboxSlideHtml = '<div class="gslide">\n    <div class="gslide-inner-content">\n        <div class="ginner-container">\n            <div class="gslide-media">\n            </div>\n            <div class="gslide-description">\n                <div class="gdesc-inner">\n                    <h4 class="gslide-title"></h4>\n                    <div class="gslide-desc"></div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>';
  defaults.slideHtml = lightboxSlideHtml;
  var lightboxHtml = '<div id="glightbox-body" class="glightbox-container">\n    <div class="gloader visible"></div>\n    <div class="goverlay"></div>\n    <div class="gcontainer">\n    <div id="glightbox-slider" class="gslider"></div>\n    <button class="gnext gbtn" tabindex="0">{nextSVG}</button>\n    <button class="gprev gbtn" tabindex="1">{prevSVG}</button>\n    <button class="gclose gbtn" tabindex="2">{closeSVG}</button>\n</div>\n</div>';
  defaults.lightboxHtml = lightboxHtml;

  function extend() {
    var extended = {};
    var deep = false;
    var i = 0;
    var length = arguments.length;

    if (Object.prototype.toString.call(arguments[0]) === '[object Boolean]') {
      deep = arguments[0];
      i++;
    }

    var merge = function merge(obj) {
      for (var prop in obj) {
        if (Object.prototype.hasOwnProperty.call(obj, prop)) {
          if (deep && Object.prototype.toString.call(obj[prop]) === '[object Object]') {
            extended[prop] = extend(true, extended[prop], obj[prop]);
          } else {
            extended[prop] = obj[prop];
          }
        }
      }
    };

    for (; i < length; i++) {
      var obj = arguments[i];
      merge(obj);
    }

    return extended;
  }

  var utils = {
    isFunction: function isFunction(f) {
      return typeof f === 'function';
    },
    isString: function isString(s) {
      return typeof s === 'string';
    },
    isNode: function isNode(el) {
      return !!(el && el.nodeType && el.nodeType == 1);
    },
    isArray: function isArray(ar) {
      return Array.isArray(ar);
    },
    isArrayLike: function isArrayLike(ar) {
      return ar && ar.length && isFinite(ar.length);
    },
    isObject: function isObject(o) {
      var type = _typeof(o);

      return type === 'object' && o != null && !utils.isFunction(o) && !utils.isArray(o);
    },
    isNil: function isNil(o) {
      return o == null;
    },
    has: function has(obj, key) {
      return obj !== null && hasOwnProperty.call(obj, key);
    },
    size: function size(o) {
      if (utils.isObject(o)) {
        if (o.keys) {
          return o.keys().length;
        }

        var l = 0;

        for (var k in o) {
          if (utils.has(o, k)) {
            l++;
          }
        }

        return l;
      } else {
        return o.length;
      }
    },
    isNumber: function isNumber(n) {
      return !isNaN(parseFloat(n)) && isFinite(n);
    }
  };

  function each(collection, callback) {
    if (utils.isNode(collection) || collection === window || collection === document) {
      collection = [collection];
    }

    if (!utils.isArrayLike(collection) && !utils.isObject(collection)) {
      collection = [collection];
    }

    if (utils.size(collection) == 0) {
      return;
    }

    if (utils.isArrayLike(collection) && !utils.isObject(collection)) {
      var l = collection.length,
          i = 0;

      for (; i < l; i++) {
        if (callback.call(collection[i], collection[i], i, collection) === false) {
          break;
        }
      }
    } else if (utils.isObject(collection)) {
      for (var key in collection) {
        if (utils.has(collection, key)) {
          if (callback.call(collection[key], collection[key], key, collection) === false) {
            break;
          }
        }
      }
    }
  }

  function getNodeEvents(node) {
    var name = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
    var fn = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
    var cache = node[uid] = node[uid] || [];
    var data = {
      all: cache,
      evt: null,
      found: null
    };

    if (name && fn && utils.size(cache) > 0) {
      each(cache, function (cl, i) {
        if (cl.eventName == name && cl.fn.toString() == fn.toString()) {
          data.found = true;
          data.evt = i;
          return false;
        }
      });
    }

    return data;
  }

  function addEvent(eventName) {
    var _ref = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {},
        onElement = _ref.onElement,
        withCallback = _ref.withCallback,
        _ref$avoidDuplicate = _ref.avoidDuplicate,
        avoidDuplicate = _ref$avoidDuplicate === void 0 ? true : _ref$avoidDuplicate,
        _ref$once = _ref.once,
        once = _ref$once === void 0 ? false : _ref$once,
        _ref$useCapture = _ref.useCapture,
        useCapture = _ref$useCapture === void 0 ? false : _ref$useCapture;

    var thisArg = arguments.length > 2 ? arguments[2] : undefined;
    var element = onElement || [];

    if (utils.isString(element)) {
      element = document.querySelectorAll(element);
    }

    function handler(event) {
      if (utils.isFunction(withCallback)) {
        withCallback.call(thisArg, event, this);
      }

      if (once) {
        handler.destroy();
      }
    }

    handler.destroy = function () {
      each(element, function (el) {
        var events = getNodeEvents(el, eventName, handler);

        if (events.found) {
          events.all.splice(events.evt, 1);
        }

        if (el.removeEventListener) el.removeEventListener(eventName, handler, useCapture);
      });
    };

    each(element, function (el) {
      var events = getNodeEvents(el, eventName, handler);

      if (el.addEventListener && avoidDuplicate && !events.found || !avoidDuplicate) {
        el.addEventListener(eventName, handler, useCapture);
        events.all.push({
          eventName: eventName,
          fn: handler
        });
      }
    });
    return handler;
  }

  function addClass(node, name) {
    each(name.split(' '), function (cl) {
      return node.classList.add(cl);
    });
  }

  function removeClass(node, name) {
    each(name.split(' '), function (cl) {
      return node.classList.remove(cl);
    });
  }

  function hasClass(node, name) {
    return node.classList.contains(name);
  }

  function whichAnimationEvent() {
    var t,
        el = document.createElement('fakeelement');
    var animations = {
      animation: 'animationend',
      OAnimation: 'oAnimationEnd',
      MozAnimation: 'animationend',
      WebkitAnimation: 'webkitAnimationEnd'
    };

    for (t in animations) {
      if (el.style[t] !== undefined) {
        return animations[t];
      }
    }
  }

  function whichTransitionEvent() {
    var t,
        el = document.createElement('fakeelement');
    var transitions = {
      transition: 'transitionend',
      OTransition: 'oTransitionEnd',
      MozTransition: 'transitionend',
      WebkitTransition: 'webkitTransitionEnd'
    };

    for (t in transitions) {
      if (el.style[t] !== undefined) {
        return transitions[t];
      }
    }
  }

  function animateElement(element) {
    var animation = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';
    var callback = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

    if (!element || animation === '') {
      return false;
    }

    if (animation == 'none') {
      if (utils.isFunction(callback)) callback();
      return false;
    }

    var animationNames = animation.split(' ');
    each(animationNames, function (name) {
      addClass(element, 'g' + name);
    });
    addEvent(animationEnd, {
      onElement: element,
      avoidDuplicate: false,
      once: true,
      withCallback: function withCallback(event, target) {
        each(animationNames, function (name) {
          removeClass(target, 'g' + name);
        });
        if (utils.isFunction(callback)) callback();
      }
    });
  }

  function createHTML(htmlStr) {
    var frag = document.createDocumentFragment(),
        temp = document.createElement('div');
    temp.innerHTML = htmlStr;

    while (temp.firstChild) {
      frag.appendChild(temp.firstChild);
    }

    return frag;
  }

  function getClosest(elem, selector) {
    while (elem !== document.body) {
      elem = elem.parentElement;
      var matches = typeof elem.matches == 'function' ? elem.matches(selector) : elem.msMatchesSelector(selector);
      if (matches) return elem;
    }
  }

  function show(element) {
    element.style.display = 'block';
  }

  function hide(element) {
    element.style.display = 'none';
  }

  function windowSize() {
    return {
      width: window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth,
      height: window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight
    };
  }

  function handleMediaFullScreen(event) {
    if (!hasClass(event.target, 'plyr--html5')) {
      return;
    }

    var media = getClosest(event.target, '.gslide-media');

    if (event.type == 'enterfullscreen') {
      addClass(media, 'fullscreen');
    }

    if (event.type == 'exitfullscreen') {
      removeClass(media, 'fullscreen');
    }
  }

  var getSlideData = function getSlideData() {
    var element = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
    var settings = arguments.length > 1 ? arguments[1] : undefined;
    var data = {
      href: '',
      title: '',
      type: '',
      description: '',
      descPosition: settings.descPosition,
      effect: '',
      width: '',
      height: '',
      node: element
    };

    if (utils.isObject(element) && !utils.isNode(element)) {
      return extend(data, element);
    }

    var url = '';
    var config = element.getAttribute('data-glightbox');
    var nodeType = element.nodeName.toLowerCase();
    if (nodeType === 'a') url = element.href;
    if (nodeType === 'img') url = element.src;
    data.href = url;
    each(data, function (val, key) {
      if (utils.has(settings, key) && key !== 'width') {
        data[key] = settings[key];
      }

      var nodeData = element.dataset[key];

      if (!utils.isNil(nodeData)) {
        data[key] = nodeData;
      }
    });

    if (!data.type) {
      data.type = getSourceType(url);
    }

    if (!utils.isNil(config)) {
      var cleanKeys = [];
      each(data, function (v, k) {
        cleanKeys.push(';\\s?' + k);
      });
      cleanKeys = cleanKeys.join('\\s?:|');

      if (config.trim() !== '') {
        each(data, function (val, key) {
          var str = config;
          var match = 's?' + key + 's?:s?(.*?)(' + cleanKeys + 's?:|$)';
          var regex = new RegExp(match);
          var matches = str.match(regex);

          if (matches && matches.length && matches[1]) {
            var value = matches[1].trim().replace(/;\s*$/, '');
            data[key] = value;
          }
        });
      }
    } else {
      if (nodeType == 'a') {
        var title = element.title;
        if (!utils.isNil(title) && title !== '') data.title = title;
      }

      if (nodeType == 'img') {
        var alt = element.alt;
        if (!utils.isNil(alt) && alt !== '') data.title = alt;
      }

      var desc = element.getAttribute('data-description');
      if (!utils.isNil(desc) && desc !== '') data.description = desc;
    }

    if (data.description && data.description.substring(0, 1) == '.' && document.querySelector(data.description)) {
      data.description = document.querySelector(data.description).innerHTML;
    } else {
      var nodeDesc = element.querySelector('.glightbox-desc');

      if (nodeDesc) {
        data.description = nodeDesc.innerHTML;
      }
    }

    var defaultWith = data.type == 'video' ? settings.videosWidth : settings.width;
    var defaultHeight = settings.height;
    data.width = utils.has(data, 'width') && data.width !== '' ? data.width : defaultWith;
    data.height = utils.has(data, 'height') && data.height !== '' ? data.height : defaultHeight;
    return data;
  };

  var setSlideContent = function setSlideContent() {
    var _this = this;

    var slide = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
    var data = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
    var callback = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

    if (hasClass(slide, 'loaded')) {
      return false;
    }

    if (utils.isFunction(this.settings.beforeSlideLoad)) {
      this.settings.beforeSlideLoad(slide, data);
    }

    var type = data.type;
    var position = data.descPosition;
    var slideMedia = slide.querySelector('.gslide-media');
    var slideTitle = slide.querySelector('.gslide-title');
    var slideText = slide.querySelector('.gslide-desc');
    var slideDesc = slide.querySelector('.gdesc-inner');
    var finalCallback = callback;

    if (utils.isFunction(this.settings.afterSlideLoad)) {
      finalCallback = function finalCallback() {
        if (utils.isFunction(callback)) {
          callback();
        }

        _this.settings.afterSlideLoad(slide, data);
      };
    }

    if (data.title == '' && data.description == '') {
      if (slideDesc) {
        slideDesc.parentNode.parentNode.removeChild(slideDesc.parentNode);
      }
    } else {
      if (slideTitle && data.title !== '') {
        slideTitle.innerHTML = data.title;
      } else {
        slideTitle.parentNode.removeChild(slideTitle);
      }

      if (slideText && data.description !== '') {
        if (isMobile && this.settings.moreLength > 0) {
          data.smallDescription = slideShortDesc(data.description, this.settings.moreLength, this.settings.moreText);
          slideText.innerHTML = data.smallDescription;
          slideDescriptionEvents.apply(this, [slideText, data]);
        } else {
          slideText.innerHTML = data.description;
        }
      } else {
        slideText.parentNode.removeChild(slideText);
      }

      addClass(slideMedia.parentNode, 'desc-'.concat(position));
      addClass(slideDesc.parentNode, 'description-'.concat(position));
    }

    addClass(slideMedia, 'gslide-'.concat(type));
    addClass(slide, 'loaded');

    if (type === 'video') {
      addClass(slideMedia.parentNode, 'gvideo-container');
      slideMedia.insertBefore(createHTML('<div class="gvideo-wrapper"></div>'), slideMedia.firstChild);
      setSlideVideo.apply(this, [slide, data, finalCallback]);
      return;
    }

    if (type === 'external') {
      var iframe = createIframe({
        url: data.href,
        width: data.width,
        height: data.height,
        callback: finalCallback
      });
      slideMedia.parentNode.style.maxWidth = ''.concat(data.width, 'px');
      slideMedia.appendChild(iframe);
      return;
    }

    if (type === 'inline') {
      setInlineContent.apply(this, [slide, data, finalCallback]);
      return;
    }

    if (type === 'image') {
      var img = new Image();
      img.addEventListener('load', function () {
        if (!isMobile && img.naturalWidth > img.offsetWidth) {
          addClass(img, 'zoomable');
          new ZoomImages(img, slide, function () {
            _this.resize(slide);
          });
        }

        if (utils.isFunction(finalCallback)) {
          finalCallback();
        }
      }, false);
      img.src = data.href;
      slideMedia.insertBefore(img, slideMedia.firstChild);
      return;
    }

    if (utils.isFunction(finalCallback)) finalCallback();
  };

  function setSlideVideo(slide, data, callback) {
    var _this2 = this;

    var videoID = 'gvideo' + data.index;
    var slideMedia = slide.querySelector('.gvideo-wrapper');
    injectVideoApi(this.settings.plyr.css);
    var url = data.href;
    var protocol = location.protocol.replace(':', '');
    var videoSource = '';
    var embedID = '';
    var customPlaceholder = false;

    if (protocol == 'file') {
      protocol = 'http';
    }

    slideMedia.parentNode.style.maxWidth = ''.concat(data.width, 'px');
    injectVideoApi(this.settings.plyr.js, 'Plyr', function () {
      if (url.match(/vimeo\.com\/([0-9]*)/)) {
        var vimeoID = /vimeo.*\/(\d+)/i.exec(url);
        videoSource = 'vimeo';
        embedID = vimeoID[1];
      }

      if (url.match(/(youtube\.com|youtube-nocookie\.com)\/watch\?v=([a-zA-Z0-9\-_]+)/) || url.match(/youtu\.be\/([a-zA-Z0-9\-_]+)/) || url.match(/(youtube\.com|youtube-nocookie\.com)\/embed\/([a-zA-Z0-9\-_]+)/)) {
        var youtubeID = getYoutubeID(url);
        videoSource = 'youtube';
        embedID = youtubeID;
      }

      if (url.match(/\.(mp4|ogg|webm|mov)$/) !== null) {
        videoSource = 'local';

        var _html = '<video id="' + videoID + '" ';

        _html += 'style="background:#000; max-width: '.concat(data.width, 'px;" ');
        _html += 'preload="metadata" ';
        _html += 'x-webkit-airplay="allow" ';
        _html += 'webkit-playsinline="" ';
        _html += 'controls ';
        _html += 'class="gvideo-local">';
        var format = url.toLowerCase().split('.').pop();
        var sources = {
          mp4: '',
          ogg: '',
          webm: ''
        };
        format = format == 'mov' ? 'mp4' : format;
        sources[format] = url;

        for (var key in sources) {
          if (sources.hasOwnProperty(key)) {
            var videoFile = sources[key];

            if (data.hasOwnProperty(key)) {
              videoFile = data[key];
            }

            if (videoFile !== '') {
              _html += '<source src="'.concat(videoFile, '" type="video/').concat(key, '">');
            }
          }
        }

        _html += '</video>';
        customPlaceholder = createHTML(_html);
      }

      var placeholder = customPlaceholder ? customPlaceholder : createHTML('<div id="'.concat(videoID, '" data-plyr-provider="').concat(videoSource, '" data-plyr-embed-id="').concat(embedID, '"></div>'));
      addClass(slideMedia, ''.concat(videoSource, '-video gvideo'));
      slideMedia.appendChild(placeholder);
      slideMedia.setAttribute('data-id', videoID);
      var playerConfig = utils.has(_this2.settings.plyr, 'config') ? _this2.settings.plyr.config : {};
      var player = new Plyr('#' + videoID, playerConfig);
      player.on('ready', function (event) {
        var instance = event.detail.plyr;
        videoPlayers[videoID] = instance;

        if (utils.isFunction(callback)) {
          callback();
        }
      });
      player.on('enterfullscreen', handleMediaFullScreen);
      player.on('exitfullscreen', handleMediaFullScreen);
    });
  }

  function createIframe(config) {
    var url = config.url,
        width = config.width,
        height = config.height,
        allow = config.allow,
        callback = config.callback,
        appendTo = config.appendTo;
    var iframe = document.createElement('iframe');
    var winWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
    iframe.className = 'vimeo-video gvideo';
    iframe.src = url;

    if (height) {
      if (isMobile && winWidth < 767) {
        iframe.style.height = '';
      } else {
        iframe.style.height = ''.concat(height, 'px');
      }
    }

    if (width) {
      iframe.style.width = ''.concat(width, 'px');
    }

    if (allow) {
      iframe.setAttribute('allow', allow);
    }

    iframe.onload = function () {
      addClass(iframe, 'node-ready');

      if (utils.isFunction(callback)) {
        callback();
      }
    };

    if (appendTo) {
      appendTo.appendChild(iframe);
    }

    return iframe;
  }

  function getYoutubeID(url) {
    var videoID = '';
    url = url.replace(/(>|<)/gi, '').split(/(vi\/|v=|\/v\/|youtu\.be\/|\/embed\/)/);

    if (url[2] !== undefined) {
      videoID = url[2].split(/[^0-9a-z_\-]/i);
      videoID = videoID[0];
    } else {
      videoID = url;
    }

    return videoID;
  }

  function injectVideoApi(url, waitFor, callback) {
    if (utils.isNil(url)) {
      console.error('Inject videos api error');
      return;
    }

    if (utils.isFunction(waitFor)) {
      callback = waitFor;
      waitFor = false;
    }

    var found;

    if (url.indexOf('.css') !== -1) {
      found = document.querySelectorAll('link[href="' + url + '"]');

      if (found && found.length > 0) {
        if (utils.isFunction(callback)) callback();
        return;
      }

      var head = document.getElementsByTagName('head')[0];
      var headStyles = head.querySelectorAll('link[rel="stylesheet"]');
      var link = document.createElement('link');
      link.rel = 'stylesheet';
      link.type = 'text/css';
      link.href = url;
      link.media = 'all';

      if (headStyles) {
        head.insertBefore(link, headStyles[0]);
      } else {
        head.appendChild(link);
      }

      if (utils.isFunction(callback)) callback();
      return;
    }

    found = document.querySelectorAll('script[src="' + url + '"]');

    if (found && found.length > 0) {
      if (utils.isFunction(callback)) {
        if (utils.isString(waitFor)) {
          waitUntil(function () {
            return typeof window[waitFor] !== 'undefined';
          }, function () {
            callback();
          });
          return false;
        }

        callback();
      }

      return;
    }

    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = url;

    script.onload = function () {
      if (utils.isFunction(callback)) {
        if (utils.isString(waitFor)) {
          waitUntil(function () {
            return typeof window[waitFor] !== 'undefined';
          }, function () {
            callback();
          });
          return false;
        }

        callback();
      }
    };

    document.body.appendChild(script);
    return;
  }

  function waitUntil(check, onComplete, delay, timeout) {
    if (check()) {
      onComplete();
      return;
    }

    if (!delay) delay = 100;
    var timeoutPointer;
    var intervalPointer = setInterval(function () {
      if (!check()) return;
      clearInterval(intervalPointer);
      if (timeoutPointer) clearTimeout(timeoutPointer);
      onComplete();
    }, delay);
    if (timeout) timeoutPointer = setTimeout(function () {
      clearInterval(intervalPointer);
    }, timeout);
  }

  function setInlineContent(slide, data, callback) {
    var _this3 = this;

    var slideMedia = slide.querySelector('.gslide-media');
    var hash = data.href.split('#').pop().trim();
    var div = document.getElementById(hash);

    if (!div) {
      return false;
    }

    var cloned = div.cloneNode(true);
    cloned.style.height = utils.isNumber(data.height) ? ''.concat(data.height, 'px') : data.height;
    cloned.style.maxWidth = utils.isNumber(data.width) ? ''.concat(data.width, 'px') : data.width;
    addClass(cloned, 'ginlined-content');
    slideMedia.appendChild(cloned);
    this.events['inlineclose' + hash] = addEvent('click', {
      onElement: slideMedia.querySelectorAll('.gtrigger-close'),
      withCallback: function withCallback(e) {
        e.preventDefault();

        _this3.close();
      }
    });

    if (utils.isFunction(callback)) {
      callback();
    }

    return;
  }

  var getSourceType = function getSourceType(url) {
    var origin = url;
    url = url.toLowerCase();

    if (url.match(/\.(jpeg|jpg|jpe|gif|png|apn|webp|svg)$/) !== null) {
      return 'image';
    }

    if (url.match(/(youtube\.com|youtube-nocookie\.com)\/watch\?v=([a-zA-Z0-9\-_]+)/) || url.match(/youtu\.be\/([a-zA-Z0-9\-_]+)/) || url.match(/(youtube\.com|youtube-nocookie\.com)\/embed\/([a-zA-Z0-9\-_]+)/)) {
      return 'video';
    }

    if (url.match(/vimeo\.com\/([0-9]*)/)) {
      return 'video';
    }

    if (url.match(/\.(mp4|ogg|webm|mov)$/) !== null) {
      return 'video';
    }

    if (url.indexOf('#') > -1) {
      var hash = origin.split('#').pop();

      if (hash.trim() !== '') {
        return 'inline';
      }
    }

    if (url.includes('gajax=true')) {
      return 'ajax';
    }

    return 'external';
  };

  function keyboardNavigation() {
    var _this4 = this;

    if (this.events.hasOwnProperty('keyboard')) {
      return false;
    }

    this.events['keyboard'] = addEvent('keydown', {
      onElement: window,
      withCallback: function withCallback(event, target) {
        event = event || window.event;
        var key = event.keyCode;

        if (key == 9) {
          event.preventDefault();
          var btns = document.querySelectorAll('.gbtn');

          if (!btns || btns.length <= 0) {
            return;
          }

          var focused = _toConsumableArray(btns).filter(function (item) {
            return hasClass(item, 'focused');
          });

          if (!focused.length) {
            var first = document.querySelector('.gbtn[tabindex="0"]');

            if (first) {
              first.focus();
              addClass(first, 'focused');
            }

            return;
          }

          btns.forEach(function (element) {
            return removeClass(element, 'focused');
          });
          var tabindex = focused[0].getAttribute('tabindex');
          tabindex = tabindex ? tabindex : '0';
          var newIndex = parseInt(tabindex) + 1;

          if (newIndex > btns.length - 1) {
            newIndex = '0';
          }

          var next = document.querySelector('.gbtn[tabindex="'.concat(newIndex, '"]'));

          if (next) {
            next.focus();
            addClass(next, 'focused');
          }
        }

        if (key == 39) _this4.nextSlide();
        if (key == 37) _this4.prevSlide();
        if (key == 27) _this4.close();
      }
    });
  }

  function touchNavigation() {
    var _this5 = this;

    if (this.events.hasOwnProperty('touch')) {
      return false;
    }

    var winSize = windowSize();
    var winWidth = winSize.width;
    var winHeight = winSize.height;
    var process = false;
    var currentSlide = null;
    var media = null;
    var mediaImage = null;
    var doingMove = false;
    var initScale = 1;
    var maxScale = 4.5;
    var currentScale = 1;
    var doingZoom = false;
    var imageZoomed = false;
    var zoomedPosX = null;
    var zoomedPosY = null;
    var lastZoomedPosX = null;
    var lastZoomedPosY = null;
    var hDistance;
    var vDistance;
    var hDistancePercent = 0;
    var vDistancePercent = 0;
    var vSwipe = false;
    var hSwipe = false;
    var startCoords = {};
    var endCoords = {};
    var xDown = 0;
    var yDown = 0;
    var isInlined;
    var instance = this;
    var sliderWrapper = document.getElementById('glightbox-slider');
    var overlay = document.querySelector('.goverlay');
    var loop = this.loop();
    var touchInstance = new TouchEvents(sliderWrapper, {
      touchStart: function touchStart(e) {
        if (hasClass(e.targetTouches[0].target, 'ginner-container')) {
          process = false;
          return false;
        }

        process = true;
        endCoords = e.targetTouches[0];
        startCoords.pageX = e.targetTouches[0].pageX;
        startCoords.pageY = e.targetTouches[0].pageY;
        xDown = e.targetTouches[0].clientX;
        yDown = e.targetTouches[0].clientY;
        currentSlide = instance.activeSlide;
        media = currentSlide.querySelector('.gslide-media');
        isInlined = currentSlide.querySelector('.gslide-inline');
        mediaImage = null;

        if (hasClass(media, 'gslide-image')) {
          mediaImage = media.querySelector('img');
        }

        removeClass(overlay, 'greset');
      },
      touchMove: function touchMove(e) {
        if (!process) {
          return;
        }

        endCoords = e.targetTouches[0];

        if (doingZoom || imageZoomed) {
          return;
        }

        if (isInlined && isInlined.offsetHeight > winHeight) {
          var moved = startCoords.pageX - endCoords.pageX;

          if (Math.abs(moved) <= 13) {
            return false;
          }
        }

        doingMove = true;
        var xUp = e.targetTouches[0].clientX;
        var yUp = e.targetTouches[0].clientY;
        var xDiff = xDown - xUp;
        var yDiff = yDown - yUp;

        if (Math.abs(xDiff) > Math.abs(yDiff)) {
          vSwipe = false;
          hSwipe = true;
        } else {
          hSwipe = false;
          vSwipe = true;
        }

        hDistance = endCoords.pageX - startCoords.pageX;
        hDistancePercent = hDistance * 100 / winWidth;
        vDistance = endCoords.pageY - startCoords.pageY;
        vDistancePercent = vDistance * 100 / winHeight;
        var opacity;

        if (vSwipe && mediaImage) {
          opacity = 1 - Math.abs(vDistance) / winHeight;
          overlay.style.opacity = opacity;

          if (_this5.settings.touchFollowAxis) {
            hDistancePercent = 0;
          }
        }

        if (hSwipe) {
          opacity = 1 - Math.abs(hDistance) / winWidth;
          media.style.opacity = opacity;

          if (_this5.settings.touchFollowAxis) {
            vDistancePercent = 0;
          }
        }

        if (!mediaImage) {
          return slideCSSTransform(media, 'translate3d('.concat(hDistancePercent, '%, 0, 0)'));
        }

        slideCSSTransform(media, 'translate3d('.concat(hDistancePercent, '%, ').concat(vDistancePercent, '%, 0)'));
      },
      touchEnd: function touchEnd() {
        if (!process) {
          return;
        }

        doingMove = false;

        if (imageZoomed || doingZoom) {
          lastZoomedPosX = zoomedPosX;
          lastZoomedPosY = zoomedPosY;
          return;
        }

        var v = Math.abs(parseInt(vDistancePercent));
        var h = Math.abs(parseInt(hDistancePercent));

        if (v > 29 && mediaImage) {
          _this5.close();

          return;
        }

        if (v < 29 && h < 25) {
          addClass(overlay, 'greset');
          overlay.style.opacity = 1;
          return resetSlideMove(media);
        }
      },
      multipointEnd: function multipointEnd() {
        setTimeout(function () {
          doingZoom = false;
        }, 50);
      },
      multipointStart: function multipointStart() {
        doingZoom = true;
        initScale = currentScale ? currentScale : 1;
      },
      pinch: function pinch(evt) {
        if (!mediaImage || doingMove) {
          return false;
        }

        doingZoom = true;
        mediaImage.scaleX = mediaImage.scaleY = initScale * evt.zoom;
        var scale = initScale * evt.zoom;
        imageZoomed = true;

        if (scale <= 1) {
          imageZoomed = false;
          scale = 1;
          lastZoomedPosY = null;
          lastZoomedPosX = null;
          zoomedPosX = null;
          zoomedPosY = null;
          mediaImage.setAttribute('style', '');
          return;
        }

        if (scale > maxScale) {
          scale = maxScale;
        }

        mediaImage.style.transform = 'scale3d('.concat(scale, ', ').concat(scale, ', 1)');
        currentScale = scale;
      },
      pressMove: function pressMove(e) {
        if (imageZoomed && !doingZoom) {
          var mhDistance = endCoords.pageX - startCoords.pageX;
          var mvDistance = endCoords.pageY - startCoords.pageY;

          if (lastZoomedPosX) {
            mhDistance = mhDistance + lastZoomedPosX;
          }

          if (lastZoomedPosY) {
            mvDistance = mvDistance + lastZoomedPosY;
          }

          zoomedPosX = mhDistance;
          zoomedPosY = mvDistance;
          var style = 'translate3d('.concat(mhDistance, 'px, ').concat(mvDistance, 'px, 0)');

          if (currentScale) {
            style += ' scale3d('.concat(currentScale, ', ').concat(currentScale, ', 1)');
          }

          slideCSSTransform(mediaImage, style);
        }
      },
      swipe: function swipe(evt) {
        if (imageZoomed) {
          return;
        }

        if (doingZoom) {
          doingZoom = false;
          return;
        }

        if (evt.direction == 'Left') {
          if (_this5.index == _this5.elements.length - 1) {
            return resetSlideMove(media);
          }

          _this5.nextSlide();
        }

        if (evt.direction == 'Right') {
          if (_this5.index == 0) {
            return resetSlideMove(media);
          }

          _this5.prevSlide();
        }
      }
    });
    this.events['touch'] = touchInstance;
  }

  function slideCSSTransform(slide) {
    var translate = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';

    if (translate == '') {
      slide.style.webkitTransform = '';
      slide.style.MozTransform = '';
      slide.style.msTransform = '';
      slide.style.OTransform = '';
      slide.style.transform = '';
      return false;
    }

    slide.style.webkitTransform = translate;
    slide.style.MozTransform = translate;
    slide.style.msTransform = translate;
    slide.style.OTransform = translate;
    slide.style.transform = translate;
  }

  function resetSlideMove(slide) {
    var media = hasClass(slide, 'gslide-media') ? slide : slide.querySelector('.gslide-media');
    var desc = slide.querySelector('.gslide-description');
    addClass(media, 'greset');
    slideCSSTransform(media, 'translate3d(0, 0, 0)');
    var animation = addEvent(transitionEnd, {
      onElement: media,
      once: true,
      withCallback: function withCallback(event, target) {
        removeClass(media, 'greset');
      }
    });
    media.style.opacity = '';

    if (desc) {
      desc.style.opacity = '';
    }
  }

  function slideShortDesc(string) {
    var n = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 50;
    var wordBoundary = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
    var useWordBoundary = wordBoundary;
    string = string.trim();

    if (string.length <= n) {
      return string;
    }

    var subString = string.substr(0, n - 1);

    if (!useWordBoundary) {
      return subString;
    }

    return subString + '... <a href="#" class="desc-more">' + wordBoundary + '</a>';
  }

  function slideDescriptionEvents(desc, data) {
    var moreLink = desc.querySelector('.desc-more');

    if (!moreLink) {
      return false;
    }

    addEvent('click', {
      onElement: moreLink,
      withCallback: function withCallback(event, target) {
        event.preventDefault();
        var body = document.body;
        var desc = getClosest(target, '.gslide-desc');

        if (!desc) {
          return false;
        }

        desc.innerHTML = data.description;
        addClass(body, 'gdesc-open');
        var shortEvent = addEvent('click', {
          onElement: [body, getClosest(desc, '.gslide-description')],
          withCallback: function withCallback(event, target) {
            if (event.target.nodeName.toLowerCase() !== 'a') {
              removeClass(body, 'gdesc-open');
              addClass(body, 'gdesc-closed');
              desc.innerHTML = data.smallDescription;
              slideDescriptionEvents(desc, data);
              setTimeout(function () {
                removeClass(body, 'gdesc-closed');
              }, 400);
              shortEvent.destroy();
            }
          }
        });
      }
    });
  }

  var GlightboxInit = function () {
    function GlightboxInit(options) {
      _classCallCheck(this, GlightboxInit);

      this.settings = extend(defaults, options || {});
      this.effectsClasses = this.getAnimationClasses();
      this.slidesData = {};
    }

    _createClass(GlightboxInit, [{
      key: 'init',
      value: function init() {
        var _this6 = this;

        this.baseEvents = addEvent('click', {
          onElement: '.'.concat(this.settings.selector),
          withCallback: function withCallback(e, target) {
            e.preventDefault();

            _this6.open(target);
          }
        });
      }
    }, {
      key: 'open',
      value: function open() {
        var element = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
        this.elements = this.getElements(element);
        if (this.elements.length == 0) return false;
        this.activeSlide = null;
        this.prevActiveSlideIndex = null;
        this.prevActiveSlide = null;
        var index = this.settings.startAt;

        if (element && utils.isNil(index)) {
          index = this.elements.indexOf(element);

          if (index < 0) {
            index = 0;
          }
        }

        if (utils.isNil(index)) {
          index = 0;
        }

        this.build();
        animateElement(this.overlay, this.settings.openEffect == 'none' ? 'none' : this.settings.cssEfects.fade['in']);
        var body = document.body;
        body.style.width = ''.concat(body.offsetWidth, 'px');
        addClass(body, 'glightbox-open');
        addClass(html, 'glightbox-open');

        if (isMobile) {
          addClass(document.body, 'glightbox-mobile');
          this.settings.slideEffect = 'slide';
        }

        this.showSlide(index, true);

        if (this.elements.length == 1) {
          hide(this.prevButton);
          hide(this.nextButton);
        } else {
          show(this.prevButton);
          show(this.nextButton);
        }

        this.lightboxOpen = true;

        if (utils.isFunction(this.settings.onOpen)) {
          this.settings.onOpen();
        }

        if (isMobile && isTouch && this.settings.touchNavigation) {
          touchNavigation.apply(this);
          return false;
        }

        if (this.settings.keyboardNavigation) {
          keyboardNavigation.apply(this);
        }
      }
    }, {
      key: 'showSlide',
      value: function showSlide() {
        var _this7 = this;

        var index = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
        var first = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
        show(this.loader);
        this.index = parseInt(index);
        var current = this.slidesContainer.querySelector('.current');

        if (current) {
          removeClass(current, 'current');
        }

        this.slideAnimateOut();
        var slide = this.slidesContainer.querySelectorAll('.gslide')[index];

        if (hasClass(slide, 'loaded')) {
          this.slideAnimateIn(slide, first);
          hide(this.loader);
        } else {
          show(this.loader);
          var slideData = getSlideData(this.elements[index], this.settings);
          slideData.index = index;
          this.slidesData[index] = slideData;
          setSlideContent.apply(this, [slide, slideData, function () {
            hide(_this7.loader);

            _this7.resize();

            _this7.slideAnimateIn(slide, first);
          }]);
        }

        this.slideDescription = slide.querySelector('.gslide-description');
        this.slideDescriptionContained = this.slideDescription && hasClass(this.slideDescription.parentNode, 'gslide-media');
        this.preloadSlide(index + 1);
        this.preloadSlide(index - 1);
        var loop = this.loop();
        removeClass(this.nextButton, 'disabled');
        removeClass(this.prevButton, 'disabled');

        if (index === 0 && !loop) {
          addClass(this.prevButton, 'disabled');
        } else if (index === this.elements.length - 1 && !loop) {
          addClass(this.nextButton, 'disabled');
        }

        this.activeSlide = slide;
      }
    }, {
      key: 'preloadSlide',
      value: function preloadSlide(index) {
        var _this8 = this;

        if (index < 0 || index > this.elements.length) return false;
        if (utils.isNil(this.elements[index])) return false;
        var slide = this.slidesContainer.querySelectorAll('.gslide')[index];

        if (hasClass(slide, 'loaded')) {
          return false;
        }

        var slideData = getSlideData(this.elements[index], this.settings);
        slideData.index = index;
        this.slidesData[index] = slideData;
        var type = slideData.sourcetype;

        if (type == 'video' || type == 'external') {
          setTimeout(function () {
            setSlideContent.apply(_this8, [slide, slideData]);
          }, 200);
        } else {
          setSlideContent.apply(this, [slide, slideData]);
        }
      }
    }, {
      key: 'prevSlide',
      value: function prevSlide() {
        this.goToSlide(this.index - 1);
      }
    }, {
      key: 'nextSlide',
      value: function nextSlide() {
        this.goToSlide(this.index + 1);
      }
    }, {
      key: 'goToSlide',
      value: function goToSlide() {
        var index = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
        this.prevActiveSlide = this.activeSlide;
        this.prevActiveSlideIndex = this.index;
        var loop = this.loop();

        if (!loop && (index < 0 || index > this.elements.length)) {
          return false;
        }

        if (index < 0) {
          index = this.elements.length - 1;
        } else if (index >= this.elements.length) {
          index = 0;
        }

        this.showSlide(index);
      }
    }, {
      key: 'slideAnimateIn',
      value: function slideAnimateIn(slide, first) {
        var _this9 = this;

        var slideMedia = slide.querySelector('.gslide-media');
        var slideDesc = slide.querySelector('.gslide-description');
        var prevData = {
          index: this.prevActiveSlideIndex,
          slide: this.prevActiveSlide
        };
        var nextData = {
          index: this.index,
          slide: this.activeSlide
        };

        if (slideMedia.offsetWidth > 0 && slideDesc) {
          hide(slideDesc);
          slideDesc.style.display = '';
        }

        removeClass(slide, this.effectsClasses);

        if (first) {
          animateElement(slide, this.settings.openEffect, function () {
            if (!isMobile && _this9.settings.autoplayVideos) {
              _this9.playSlideVideo(slide);
            }

            if (utils.isFunction(_this9.settings.afterSlideChange)) {
              _this9.settings.afterSlideChange.apply(_this9, [prevData, nextData]);
            }
          });
        } else {
          var effect_name = this.settings.slideEffect;
          var animIn = effect_name !== 'none' ? this.settings.cssEfects[effect_name]['in'] : effect_name;

          if (this.prevActiveSlideIndex > this.index) {
            if (this.settings.slideEffect == 'slide') {
              animIn = this.settings.cssEfects.slide_back['in'];
            }
          }

          animateElement(slide, animIn, function () {
            if (!isMobile && _this9.settings.autoplayVideos) {
              _this9.playSlideVideo(slide);
            }

            if (utils.isFunction(_this9.settings.afterSlideChange)) {
              _this9.settings.afterSlideChange.apply(_this9, [prevData, nextData]);
            }
          });
        }

        setTimeout(function () {
          _this9.resize(slide);
        }, 100);
        addClass(slide, 'current');
      }
    }, {
      key: 'slideAnimateOut',
      value: function slideAnimateOut() {
        if (!this.prevActiveSlide) {
          return false;
        }

        var prevSlide = this.prevActiveSlide;
        removeClass(prevSlide, this.effectsClasses);
        addClass(prevSlide, 'prev');
        var animation = this.settings.slideEffect;
        var animOut = animation !== 'none' ? this.settings.cssEfects[animation].out : animation;
        this.stopSlideVideo(prevSlide);

        if (utils.isFunction(this.settings.beforeSlideChange)) {
          this.settings.beforeSlideChange.apply(this, [{
            index: this.prevActiveSlideIndex,
            slide: this.prevActiveSlide
          }, {
            index: this.index,
            slide: this.activeSlide
          }]);
        }

        if (this.prevActiveSlideIndex > this.index && this.settings.slideEffect == 'slide') {
          animOut = this.settings.cssEfects.slide_back.out;
        }

        animateElement(prevSlide, animOut, function () {
          var media = prevSlide.querySelector('.gslide-media');
          var desc = prevSlide.querySelector('.gslide-description');
          media.style.transform = '';
          removeClass(media, 'greset');
          media.style.opacity = '';

          if (desc) {
            desc.style.opacity = '';
          }

          removeClass(prevSlide, 'prev');
        });
      }
    }, {
      key: 'stopSlideVideo',
      value: function stopSlideVideo(slide) {
        if (utils.isNumber(slide)) {
          slide = this.slidesContainer.querySelectorAll('.gslide')[slide];
        }

        var slideVideo = slide ? slide.querySelector('.gvideo') : null;

        if (!slideVideo) {
          return false;
        }

        var videoID = slideVideo.getAttribute('data-id');

        if (videoPlayers && utils.has(videoPlayers, videoID)) {
          var api = videoPlayers[videoID];

          if (api && api.play) {
            api.pause();
          }
        }
      }
    }, {
      key: 'playSlideVideo',
      value: function playSlideVideo(slide) {
        if (utils.isNumber(slide)) {
          slide = this.slidesContainer.querySelectorAll('.gslide')[slide];
        }

        var slideVideo = slide.querySelector('.gvideo');

        if (!slideVideo) {
          return false;
        }

        var videoID = slideVideo.getAttribute('data-id');

        if (videoPlayers && utils.has(videoPlayers, videoID)) {
          var api = videoPlayers[videoID];

          if (api && api.play) {
            api.play();
          }
        }
      }
    }, {
      key: 'setElements',
      value: function setElements(elements) {
        this.settings.elements = elements;
      }
    }, {
      key: 'getElements',
      value: function getElements() {
        var element = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
        this.elements = [];

        if (!utils.isNil(this.settings.elements) && utils.isArray(this.settings.elements)) {
          return this.settings.elements;
        }

        var nodes = false;

        if (element !== null) {
          var gallery = element.getAttribute('data-gallery');

          if (gallery && gallery !== '') {
            nodes = document.querySelectorAll('[data-gallery="'.concat(gallery, '"]'));
          }
        }

        if (nodes == false) {
          nodes = document.querySelectorAll('.'.concat(this.settings.selector));
        }

        nodes = Array.prototype.slice.call(nodes);
        return nodes;
      }
    }, {
      key: 'getActiveSlide',
      value: function getActiveSlide() {
        return this.slidesContainer.querySelectorAll('.gslide')[this.index];
      }
    }, {
      key: 'getActiveSlideIndex',
      value: function getActiveSlideIndex() {
        return this.index;
      }
    }, {
      key: 'getAnimationClasses',
      value: function getAnimationClasses() {
        var effects = [];

        for (var key in this.settings.cssEfects) {
          if (this.settings.cssEfects.hasOwnProperty(key)) {
            var effect = this.settings.cssEfects[key];
            effects.push('g'.concat(effect['in']));
            effects.push('g'.concat(effect.out));
          }
        }

        return effects.join(' ');
      }
    }, {
      key: 'build',
      value: function build() {
        var _this10 = this;

        if (this.built) {
          return false;
        }

        var nextSVG = utils.has(this.settings.svg, 'next') ? this.settings.svg.next : '';
        var prevSVG = utils.has(this.settings.svg, 'prev') ? this.settings.svg.prev : '';
        var closeSVG = utils.has(this.settings.svg, 'close') ? this.settings.svg.close : '';
        var lightboxHTML = this.settings.lightboxHtml;
        lightboxHTML = lightboxHTML.replace(/{nextSVG}/g, nextSVG);
        lightboxHTML = lightboxHTML.replace(/{prevSVG}/g, prevSVG);
        lightboxHTML = lightboxHTML.replace(/{closeSVG}/g, closeSVG);
        lightboxHTML = createHTML(lightboxHTML);
        document.body.appendChild(lightboxHTML);
        var modal = document.getElementById('glightbox-body');
        this.modal = modal;
        var closeButton = modal.querySelector('.gclose');
        this.prevButton = modal.querySelector('.gprev');
        this.nextButton = modal.querySelector('.gnext');
        this.overlay = modal.querySelector('.goverlay');
        this.loader = modal.querySelector('.gloader');
        this.slidesContainer = document.getElementById('glightbox-slider');
        this.events = {};
        addClass(this.modal, 'glightbox-' + this.settings.skin);

        if (this.settings.closeButton && closeButton) {
          this.events['close'] = addEvent('click', {
            onElement: closeButton,
            withCallback: function withCallback(e, target) {
              e.preventDefault();

              _this10.close();
            }
          });
        }

        if (closeButton && !this.settings.closeButton) {
          closeButton.parentNode.removeChild(closeButton);
        }

        if (this.nextButton) {
          this.events['next'] = addEvent('click', {
            onElement: this.nextButton,
            withCallback: function withCallback(e, target) {
              e.preventDefault();

              _this10.nextSlide();
            }
          });
        }

        if (this.prevButton) {
          this.events['prev'] = addEvent('click', {
            onElement: this.prevButton,
            withCallback: function withCallback(e, target) {
              e.preventDefault();

              _this10.prevSlide();
            }
          });
        }

        if (this.settings.closeOnOutsideClick) {
          this.events['outClose'] = addEvent('click', {
            onElement: modal,
            withCallback: function withCallback(e, target) {
              if (!hasClass(document.body, 'glightbox-mobile') && !getClosest(e.target, '.ginner-container')) {
                if (!getClosest(e.target, '.gbtn') && !hasClass(e.target, 'gnext') && !hasClass(e.target, 'gprev')) {
                  _this10.close();
                }
              }
            }
          });
        }

        each(this.elements, function () {
          var slide = createHTML(_this10.settings.slideHtml);

          _this10.slidesContainer.appendChild(slide);
        });

        if (isTouch) {
          addClass(document.body, 'glightbox-touch');
        }

        this.events['resize'] = addEvent('resize', {
          onElement: window,
          withCallback: function withCallback() {
            _this10.resize();
          }
        });
        this.built = true;
      }
    }, {
      key: 'resize',
      value: function resize() {
        var slide = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
        slide = !slide ? this.activeSlide : slide;
        document.body.style.width = '';
        document.body.style.width = ''.concat(document.body.offsetWidth, 'px');

        if (!slide || hasClass(slide, 'zoomed')) {
          return;
        }

        var winSize = windowSize();
        var video = slide.querySelector('.gvideo-wrapper');
        var image = slide.querySelector('.gslide-image');
        var description = this.slideDescription;
        var winWidth = winSize.width;
        var winHeight = winSize.height;

        if (winWidth <= 768) {
          addClass(document.body, 'glightbox-mobile');
        } else {
          removeClass(document.body, 'glightbox-mobile');
        }

        if (!video && !image) {
          return;
        }

        var descriptionResize = false;

        if (description && (hasClass(description, 'description-bottom') || hasClass(description, 'description-top')) && !hasClass(description, 'gabsolute')) {
          descriptionResize = true;
        }

        if (image) {
          if (winWidth <= 768) {
            var imgNode = image.querySelector('img');
            imgNode.setAttribute('style', '');
          } else if (descriptionResize) {
            var descHeight = description.offsetHeight;
            var maxWidth = this.slidesData[this.index].width;
            maxWidth = maxWidth <= winWidth ? maxWidth + 'px' : '100%';

            var _imgNode = image.querySelector('img');

            _imgNode.setAttribute('style', 'max-height: calc(100vh - '.concat(descHeight, 'px)'));

            description.setAttribute('style', 'max-width: '.concat(_imgNode.offsetWidth, 'px;'));
          }
        }

        if (video) {
          var videoRatio = this.settings.plyr.ratio.split(':');
          var _maxWidth = this.slidesData[this.index].width;

          var maxHeight = _maxWidth / (parseInt(videoRatio[0]) / parseInt(videoRatio[1]));

          maxHeight = Math.floor(maxHeight);

          if (descriptionResize) {
            winHeight = winHeight - description.offsetHeight;
          }

          if (winHeight < maxHeight && winWidth > _maxWidth) {
            var vwidth = video.offsetWidth;
            var vheight = video.offsetHeight;
            var ratio = winHeight / vheight;
            var vsize = {
              width: vwidth * ratio,
              height: vheight * ratio
            };
            video.parentNode.setAttribute('style', 'max-width: '.concat(vsize.width, 'px'));

            if (descriptionResize) {
              description.setAttribute('style', 'max-width: '.concat(vsize.width, 'px;'));
            }
          } else {
            video.parentNode.style.maxWidth = ''.concat(_maxWidth, 'px');

            if (descriptionResize) {
              description.setAttribute('style', 'max-width: '.concat(_maxWidth, 'px;'));
            }
          }
        }
      }
    }, {
      key: 'reload',
      value: function reload() {
        this.init();
      }
    }, {
      key: 'loop',
      value: function loop() {
        var loop = utils.has(this.settings, 'loopAtEnd') ? this.settings.loopAtEnd : null;
        loop = utils.has(this.settings, 'loop') ? this.settings.loop : loop;
        return loop;
      }
    }, {
      key: 'close',
      value: function close() {
        var _this11 = this;

        if (this.closing) {
          return false;
        }

        this.closing = true;
        this.stopSlideVideo(this.activeSlide);
        addClass(this.modal, 'glightbox-closing');
        animateElement(this.overlay, this.settings.openEffect == 'none' ? 'none' : this.settings.cssEfects.fade.out);
        animateElement(this.activeSlide, this.settings.closeEffect, function () {
          _this11.activeSlide = null;
          _this11.prevActiveSlideIndex = null;
          _this11.prevActiveSlide = null;
          _this11.built = false;

          if (_this11.events) {
            for (var key in _this11.events) {
              if (_this11.events.hasOwnProperty(key)) {
                _this11.events[key].destroy();
              }
            }

            _this11.events = null;
          }

          var body = document.body;
          removeClass(html, 'glightbox-open');
          removeClass(body, 'glightbox-open touching gdesc-open glightbox-touch glightbox-mobile');
          body.style.width = '';

          _this11.modal.parentNode.removeChild(_this11.modal);

          if (utils.isFunction(_this11.settings.onClose)) {
            _this11.settings.onClose();
          }

          _this11.closing = null;
        });
      }
    }, {
      key: 'destroy',
      value: function destroy() {
        this.close();
        this.baseEvents.destroy();
      }
    }]);

    return GlightboxInit;
  }();

  function glightbox() {
    var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
    var instance = new GlightboxInit(options);
    instance.init();
    return instance;
  }

  return glightbox;
});

const lightbox = GLightbox({
  touchNavigation: true,
  loopAtEnd: true,
  autoplayVideos: true,
  onOpen: () => {
    console.log('Lightbox opened');
  }
});
window.onscroll = function () {
  myFunction();
};

function myFunction() {
  var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
  var height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
  var scrolled = winScroll / height * 100;
  document.getElementById("readingIndicator").value = scrolled;
}
/*! modernizr 3.8.0 (Custom Build) | MIT *
 * https://modernizr.com/download/?-cssanimations-csscolumns-customelements-flexbox-history-picture-pointerevents-postmessage-sizes-srcset-webgl-websockets-webworkers-addtest-domprefixes-hasevent-mq-prefixedcssvalue-prefixes-setclasses-testallprops-testprop-teststyles !*/
!function (e, t, n) {
  function r(e, t) {
    return typeof e === t;
  }

  function o(e) {
    var t = b.className,
        n = Modernizr._config.classPrefix || '';

    if (S && (t = t.baseVal), Modernizr._config.enableJSClass) {
      var r = new RegExp('(^|\\s)' + n + 'no-js(\\s|$)');
      t = t.replace(r, '$1' + n + 'js$2');
    }

    Modernizr._config.enableClasses && (e.length > 0 && (t += ' ' + n + e.join(' ' + n)), S ? b.className.baseVal = t : b.className = t);
  }

  function i(e, t) {
    if ('object' == typeof e) for (var n in e) P(e, n) && i(n, e[n]);else {
      e = e.toLowerCase();
      var r = e.split('.'),
          s = Modernizr[r[0]];
      if (2 === r.length && (s = s[r[1]]), void 0 !== s) return Modernizr;
      t = 'function' == typeof t ? t() : t, 1 === r.length ? Modernizr[r[0]] = t : (!Modernizr[r[0]] || Modernizr[r[0]] instanceof Boolean || (Modernizr[r[0]] = new Boolean(Modernizr[r[0]])), Modernizr[r[0]][r[1]] = t), o([(t && !1 !== t ? '' : 'no-') + r.join('-')]), Modernizr._trigger(e, t);
    }
    return Modernizr;
  }

  function s() {
    return 'function' != typeof t.createElement ? t.createElement(arguments[0]) : S ? t.createElementNS.call(t, 'http://www.w3.org/2000/svg', arguments[0]) : t.createElement.apply(t, arguments);
  }

  function a() {
    var e = t.body;
    return e || (e = s(S ? 'svg' : 'body'), e.fake = !0), e;
  }

  function l(e, n, r, o) {
    var i,
        l,
        u,
        f,
        c = 'modernizr',
        d = s('div'),
        p = a();
    if (parseInt(r, 10)) for (; r--;) u = s('div'), u.id = o ? o[r] : c + (r + 1), d.appendChild(u);
    return i = s('style'), i.type = 'text/css', i.id = 's' + c, (p.fake ? p : d).appendChild(i), p.appendChild(d), i.styleSheet ? i.styleSheet.cssText = e : i.appendChild(t.createTextNode(e)), d.id = c, p.fake && (p.style.background = '', p.style.overflow = 'hidden', f = b.style.overflow, b.style.overflow = 'hidden', b.appendChild(p)), l = n(d, e), p.fake ? (p.parentNode.removeChild(p), b.style.overflow = f, b.offsetHeight) : d.parentNode.removeChild(d), !!l;
  }

  function u(e, t) {
    return !!~('' + e).indexOf(t);
  }

  function f(e) {
    return e.replace(/([A-Z])/g, function (e, t) {
      return '-' + t.toLowerCase();
    }).replace(/^ms-/, '-ms-');
  }

  function c(t, n, r) {
    var o;

    if ('getComputedStyle' in e) {
      o = getComputedStyle.call(e, t, n);
      var i = e.console;
      if (null !== o) r && (o = o.getPropertyValue(r));else if (i) {
        var s = i.error ? 'error' : 'log';
        i[s].call(i, 'getComputedStyle returning null, its possible modernizr test results are inaccurate');
      }
    } else o = !n && t.currentStyle && t.currentStyle[r];

    return o;
  }

  function d(t, r) {
    var o = t.length;

    if ('CSS' in e && 'supports' in e.CSS) {
      for (; o--;) if (e.CSS.supports(f(t[o]), r)) return !0;

      return !1;
    }

    if ('CSSSupportsRule' in e) {
      for (var i = []; o--;) i.push('(' + f(t[o]) + ':' + r + ')');

      return i = i.join(' or '), l('@supports (' + i + ') { #modernizr { position: absolute; } }', function (e) {
        return 'absolute' === c(e, null, 'position');
      });
    }

    return n;
  }

  function p(e) {
    return e.replace(/([a-z])-([a-z])/g, function (e, t, n) {
      return t + n.toUpperCase();
    }).replace(/^-/, '');
  }

  function m(e, t, o, i) {
    function a() {
      f && (delete L.style, delete L.modElem);
    }

    if (i = !r(i, 'undefined') && i, !r(o, 'undefined')) {
      var l = d(e, o);
      if (!r(l, 'undefined')) return l;
    }

    for (var f, c, m, h, A, v = ['modernizr', 'tspan', 'samp']; !L.style && v.length;) f = !0, L.modElem = s(v.shift()), L.style = L.modElem.style;

    for (m = e.length, c = 0; c < m; c++) if (h = e[c], A = L.style[h], u(h, '-') && (h = p(h)), L.style[h] !== n) {
      if (i || r(o, 'undefined')) return a(), 'pfx' !== t || h;

      try {
        L.style[h] = o;
      } catch (e) {}

      if (L.style[h] !== A) return a(), 'pfx' !== t || h;
    }

    return a(), !1;
  }

  function h(e, t) {
    return function () {
      return e.apply(t, arguments);
    };
  }

  function A(e, t, n) {
    var o;

    for (var i in e) if (e[i] in t) return !1 === n ? e[i] : (o = t[e[i]], r(o, 'function') ? h(o, n || t) : o);

    return !1;
  }

  function v(e, t, n, o, i) {
    var s = e.charAt(0).toUpperCase() + e.slice(1),
        a = (e + ' ' + z.join(s + ' ') + s).split(' ');
    return r(t, 'string') || r(t, 'undefined') ? m(a, t, o, i) : (a = (e + ' ' + x.join(s + ' ') + s).split(' '), A(a, t, n));
  }

  function g(e, t, r) {
    return v(e, n, n, t, r);
  }

  var y = [],
      C = {
    _version: '3.8.0',
    _config: {
      classPrefix: '',
      enableClasses: !0,
      enableJSClass: !0,
      usePrefixes: !0
    },
    _q: [],
    on: function (e, t) {
      var n = this;
      setTimeout(function () {
        t(n[e]);
      }, 0);
    },
    addTest: function (e, t, n) {
      y.push({
        name: e,
        fn: t,
        options: n
      });
    },
    addAsyncTest: function (e) {
      y.push({
        name: null,
        fn: e
      });
    }
  },
      Modernizr = function () {};

  Modernizr.prototype = C, Modernizr = new Modernizr();
  var w = [],
      b = t.documentElement,
      S = 'svg' === b.nodeName.toLowerCase(),
      _ = 'Moz O ms Webkit',
      x = C._config.usePrefixes ? _.toLowerCase().split(' ') : [];
  C._domPrefixes = x;
  var T = C._config.usePrefixes ? ' -webkit- -moz- -o- -ms- '.split(' ') : ['', ''];
  C._prefixes = T;
  var P;
  !function () {
    var e = {}.hasOwnProperty;
    P = r(e, 'undefined') || r(e.call, 'undefined') ? function (e, t) {
      return t in e && r(e.constructor.prototype[t], 'undefined');
    } : function (t, n) {
      return e.call(t, n);
    };
  }(), C._l = {}, C.on = function (e, t) {
    this._l[e] || (this._l[e] = []), this._l[e].push(t), Modernizr.hasOwnProperty(e) && setTimeout(function () {
      Modernizr._trigger(e, Modernizr[e]);
    }, 0);
  }, C._trigger = function (e, t) {
    if (this._l[e]) {
      var n = this._l[e];
      setTimeout(function () {
        var e;

        for (e = 0; e < n.length; e++) (0, n[e])(t);
      }, 0), delete this._l[e];
    }
  }, Modernizr._q.push(function () {
    C.addTest = i;
  });

  var k = function () {
    function e(e, r) {
      var o;
      return !!e && (r && 'string' != typeof r || (r = s(r || 'div')), e = 'on' + e, o = e in r, !o && t && (r.setAttribute || (r = s('div')), r.setAttribute(e, ''), o = 'function' == typeof r[e], r[e] !== n && (r[e] = n), r.removeAttribute(e)), o);
    }

    var t = !('onblur' in b);
    return e;
  }();

  C.hasEvent = k;

  var E = function () {
    var t = e.matchMedia || e.msMatchMedia;
    return t ? function (e) {
      var n = t(e);
      return n && n.matches || !1;
    } : function (t) {
      var n = !1;
      return l('@media ' + t + ' { #modernizr { position: absolute; } }', function (t) {
        n = 'absolute' === (e.getComputedStyle ? e.getComputedStyle(t, null) : t.currentStyle).position;
      }), n;
    };
  }();

  C.mq = E;

  var B = function (e, t) {
    var n = !1,
        r = s('div'),
        o = r.style;

    if (e in o) {
      var i = x.length;

      for (o[e] = t, n = o[e]; i-- && !n;) o[e] = '-' + x[i] + '-' + t, n = o[e];
    }

    return '' === n && (n = !1), n;
  };

  C.prefixedCSSValue = B;
  var z = C._config.usePrefixes ? _.split(' ') : [];
  C._cssomPrefixes = z;
  var O = {
    elem: s('modernizr')
  };

  Modernizr._q.push(function () {
    delete O.elem;
  });

  var L = {
    style: O.elem.style
  };
  Modernizr._q.unshift(function () {
    delete L.style;
  }), C.testAllProps = v, C.testAllProps = g;
  C.testProp = function (e, t, r) {
    return m([e], n, t, r);
  }, C.testStyles = l;
  Modernizr.addTest('customelements', 'customElements' in e), Modernizr.addTest('history', function () {
    var t = navigator.userAgent;
    return !!t && (-1 === t.indexOf('Android 2.') && -1 === t.indexOf('Android 4.0') || -1 === t.indexOf('Mobile Safari') || -1 !== t.indexOf('Chrome') || -1 !== t.indexOf('Windows Phone') || 'file:' === location.protocol) && e.history && 'pushState' in e.history;
  });
  var N = [''].concat(x);
  C._domPrefixesAll = N, Modernizr.addTest('pointerevents', function () {
    for (var e = 0, t = N.length; e < t; e++) if (k(N[e] + 'pointerdown')) return !0;

    return !1;
  });
  var R = !0;

  try {
    e.postMessage({
      toString: function () {
        R = !1;
      }
    }, '*');
  } catch (e) {}

  Modernizr.addTest('postmessage', new Boolean('postMessage' in e)), Modernizr.addTest('postmessage.structuredclones', R), Modernizr.addTest('webgl', function () {
    return 'WebGLRenderingContext' in e;
  });
  var j = !1;

  try {
    j = 'WebSocket' in e && 2 === e.WebSocket.CLOSING;
  } catch (e) {}

  Modernizr.addTest('websockets', j), Modernizr.addTest('cssanimations', g('animationName', 'a', !0)), function () {
    Modernizr.addTest('csscolumns', function () {
      var e = !1,
          t = g('columnCount');

      try {
        e = !!t, e && (e = new Boolean(e));
      } catch (e) {}

      return e;
    });

    for (var e, t, n = ['Width', 'Span', 'Fill', 'Gap', 'Rule', 'RuleColor', 'RuleStyle', 'RuleWidth', 'BreakBefore', 'BreakAfter', 'BreakInside'], r = 0; r < n.length; r++) e = n[r].toLowerCase(), t = g('column' + n[r]), 'breakbefore' !== e && 'breakafter' !== e && 'breakinside' !== e || (t = t || g(n[r])), Modernizr.addTest('csscolumns.' + e, t);
  }(), Modernizr.addTest('flexbox', g('flexBasis', '1px', !0)), Modernizr.addTest('picture', 'HTMLPictureElement' in e), Modernizr.addAsyncTest(function () {
    var e,
        t,
        n,
        r = s('img'),
        o = ('sizes' in r);
    !o && 'srcset' in r ? (t = 'data:image/gif;base64,R0lGODlhAgABAPAAAP///wAAACH5BAAAAAAALAAAAAACAAEAAAICBAoAOw==', e = 'data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==', n = function () {
      i('sizes', 2 === r.width);
    }, r.onload = n, r.onerror = n, r.setAttribute('sizes', '9px'), r.srcset = e + ' 1w,' + t + ' 8w', r.src = e) : i('sizes', o);
  }), Modernizr.addTest('srcset', 'srcset' in s('img')), Modernizr.addTest('webworkers', 'Worker' in e), function () {
    var e, t, n, o, i, s, a;

    for (var l in y) if (y.hasOwnProperty(l)) {
      if (e = [], t = y[l], t.name && (e.push(t.name.toLowerCase()), t.options && t.options.aliases && t.options.aliases.length)) for (n = 0; n < t.options.aliases.length; n++) e.push(t.options.aliases[n].toLowerCase());

      for (o = r(t.fn, 'function') ? t.fn() : t.fn, i = 0; i < e.length; i++) s = e[i], a = s.split('.'), 1 === a.length ? Modernizr[a[0]] = o : (Modernizr[a[0]] && (!Modernizr[a[0]] || Modernizr[a[0]] instanceof Boolean) || (Modernizr[a[0]] = new Boolean(Modernizr[a[0]])), Modernizr[a[0]][a[1]] = o), w.push((o ? '' : 'no-') + a.join('-'));
    }
  }(), o(w), delete C.addTest, delete C.addAsyncTest;

  for (var M = 0; M < Modernizr._q.length; M++) Modernizr._q[M]();

  e.Modernizr = Modernizr;
}(window, document);
/**
 * File skip-link-focus-fix.js.
 *
 * Helps with accessibility for keyboard only users.
 *
 * Learn more: https://git.io/vWdr2
 */
(function () {
  var isWebkit = navigator.userAgent.toLowerCase().indexOf('webkit') > -1,
      isOpera = navigator.userAgent.toLowerCase().indexOf('opera') > -1,
      isIe = navigator.userAgent.toLowerCase().indexOf('msie') > -1;

  if ((isWebkit || isOpera || isIe) && document.getElementById && window.addEventListener) {
    window.addEventListener('hashchange', function () {
      var id = location.hash.substring(1),
          element;

      if (!/^[A-z0-9_-]+$/.test(id)) {
        return;
      }

      element = document.getElementById(id);

      if (element) {
        if (!/^(?:a|select|input|button|textarea)$/i.test(element.tagName)) {
          element.tabIndex = -1;
        }

        element.focus();
      }
    }, false);
  }
})();
window.onload = function () {
  document.body.classList.remove('loading');
  document.getElementById('loader').classList.remove('loading');
};
/*! instant.page v3.0.0 - (C) 2019 Alexandre Dieulot - https://instant.page/license */
let mouseoverTimer;
let lastTouchTimestamp;
const prefetches = new Set();
const prefetchElement = document.createElement('link');
const isSupported = prefetchElement.relList && prefetchElement.relList.supports && prefetchElement.relList.supports('prefetch') && window.IntersectionObserver && 'isIntersecting' in IntersectionObserverEntry.prototype;
const allowQueryString = ('instantAllowQueryString' in document.body.dataset);
const allowExternalLinks = ('instantAllowExternalLinks' in document.body.dataset);
const useWhitelist = ('instantWhitelist' in document.body.dataset);
let delayOnHover = 65;
let useMousedown = false;
let useMousedownOnly = false;
let useViewport = false;

if ('instantIntensity' in document.body.dataset) {
  const intensity = document.body.dataset.instantIntensity;

  if (intensity.substr(0, 'mousedown'.length) == 'mousedown') {
    useMousedown = true;

    if (intensity == 'mousedown-only') {
      useMousedownOnly = true;
    }
  } else if (intensity.substr(0, 'viewport'.length) == 'viewport') {
    if (!(navigator.connection && (navigator.connection.saveData || navigator.connection.effectiveType.includes('2g')))) {
      if (intensity == 'viewport') {
        /* Biggest iPhone resolution (which we want): 414 × 896 = 370944
         * Small 7" tablet resolution (which we don’t want): 600 × 1024 = 614400
         * Note that the viewport (which we check here) is smaller than the resolution due to the UI’s chrome */
        if (document.documentElement.clientWidth * document.documentElement.clientHeight < 450000) {
          useViewport = true;
        }
      } else if (intensity == 'viewport-all') {
        useViewport = true;
      }
    }
  } else {
    const milliseconds = parseInt(intensity);

    if (!isNaN(milliseconds)) {
      delayOnHover = milliseconds;
    }
  }
}

if (isSupported) {
  const eventListenersOptions = {
    capture: true,
    passive: true
  };

  if (!useMousedownOnly) {
    document.addEventListener('touchstart', touchstartListener, eventListenersOptions);
  }

  if (!useMousedown) {
    document.addEventListener('mouseover', mouseoverListener, eventListenersOptions);
  } else {
    document.addEventListener('mousedown', mousedownListener, eventListenersOptions);
  }

  if (useViewport) {
    let triggeringFunction;

    if (window.requestIdleCallback) {
      triggeringFunction = callback => {
        requestIdleCallback(callback, {
          timeout: 1500
        });
      };
    } else {
      triggeringFunction = callback => {
        callback();
      };
    }

    triggeringFunction(() => {
      const intersectionObserver = new IntersectionObserver(entries => {
        entries.forEach(entry => {
          if (entry.isIntersecting) {
            const linkElement = entry.target;
            intersectionObserver.unobserve(linkElement);
            preload(linkElement.href);
          }
        });
      });
      document.querySelectorAll('a').forEach(linkElement => {
        if (isPreloadable(linkElement)) {
          intersectionObserver.observe(linkElement);
        }
      });
    });
  }
}

function touchstartListener(event) {
  /* Chrome on Android calls mouseover before touchcancel so `lastTouchTimestamp`
   * must be assigned on touchstart to be measured on mouseover. */
  lastTouchTimestamp = performance.now();
  const linkElement = event.target.closest('a');

  if (!isPreloadable(linkElement)) {
    return;
  }

  preload(linkElement.href);
}

function mouseoverListener(event) {
  if (performance.now() - lastTouchTimestamp < 1100) {
    return;
  }

  const linkElement = event.target.closest('a');

  if (!isPreloadable(linkElement)) {
    return;
  }

  linkElement.addEventListener('mouseout', mouseoutListener, {
    passive: true
  });
  mouseoverTimer = setTimeout(() => {
    preload(linkElement.href);
    mouseoverTimer = undefined;
  }, delayOnHover);
}

function mousedownListener(event) {
  const linkElement = event.target.closest('a');

  if (!isPreloadable(linkElement)) {
    return;
  }

  preload(linkElement.href);
}

function mouseoutListener(event) {
  if (event.relatedTarget && event.target.closest('a') == event.relatedTarget.closest('a')) {
    return;
  }

  if (mouseoverTimer) {
    clearTimeout(mouseoverTimer);
    mouseoverTimer = undefined;
  }
}

function isPreloadable(linkElement) {
  if (!linkElement || !linkElement.href) {
    return;
  }

  if (useWhitelist && !('instant' in linkElement.dataset)) {
    return;
  }

  if (!allowExternalLinks && linkElement.origin != location.origin && !('instant' in linkElement.dataset)) {
    return;
  }

  if (!['http:', 'https:'].includes(linkElement.protocol)) {
    return;
  }

  if (linkElement.protocol == 'http:' && location.protocol == 'https:') {
    return;
  }

  if (!allowQueryString && linkElement.search && !('instant' in linkElement.dataset)) {
    return;
  }

  if (linkElement.hash && linkElement.pathname + linkElement.search == location.pathname + location.search) {
    return;
  }

  if ('noInstant' in linkElement.dataset) {
    return;
  }

  return true;
}

function preload(url) {
  if (prefetches.has(url)) {
    return;
  }

  const prefetcher = document.createElement('link');
  prefetcher.rel = 'prefetch';
  prefetcher.href = url;
  document.head.appendChild(prefetcher);
  prefetches.add(url);
}
(function (window, factory) {
  var lazySizes = factory(window, window.document, Date);
  window.lazySizes = lazySizes;

  if (typeof module == 'object' && module.exports) {
    module.exports = lazySizes;
  }
})(typeof window != 'undefined' ? window : {}, function l(window, document, Date) {
  // Pass in the windoe Date function also for SSR because the Date class can be lost
  'use strict';
  /*jshint eqnull:true */

  var lazysizes, lazySizesCfg;

  (function () {
    var prop;
    var lazySizesDefaults = {
      lazyClass: 'lazyload',
      loadedClass: 'lazyloaded',
      loadingClass: 'lazyloading',
      preloadClass: 'lazypreload',
      errorClass: 'lazyerror',
      //strictClass: 'lazystrict',
      autosizesClass: 'lazyautosizes',
      srcAttr: 'data-src',
      srcsetAttr: 'data-srcset',
      sizesAttr: 'data-sizes',
      //preloadAfterLoad: false,
      minSize: 40,
      customMedia: {},
      init: true,
      expFactor: 1.5,
      hFac: 0.8,
      loadMode: 2,
      loadHidden: true,
      ricTimeout: 0,
      throttleDelay: 125
    };
    lazySizesCfg = window.lazySizesConfig || window.lazysizesConfig || {};

    for (prop in lazySizesDefaults) {
      if (!(prop in lazySizesCfg)) {
        lazySizesCfg[prop] = lazySizesDefaults[prop];
      }
    }
  })();

  if (!document || !document.getElementsByClassName) {
    return {
      init: function () {},
      cfg: lazySizesCfg,
      noSupport: true
    };
  }

  var docElem = document.documentElement;
  var supportPicture = window.HTMLPictureElement;
  var _addEventListener = 'addEventListener';
  var _getAttribute = 'getAttribute';
  /**
   * Update to bind to window because 'this' becomes null during SSR
   * builds.
   */

  var addEventListener = window[_addEventListener].bind(window);

  var setTimeout = window.setTimeout;
  var requestAnimationFrame = window.requestAnimationFrame || setTimeout;
  var requestIdleCallback = window.requestIdleCallback;
  var regPicture = /^picture$/i;
  var loadEvents = ['load', 'error', 'lazyincluded', '_lazyloaded'];
  var regClassCache = {};
  var forEach = Array.prototype.forEach;

  var hasClass = function (ele, cls) {
    if (!regClassCache[cls]) {
      regClassCache[cls] = new RegExp('(\\s|^)' + cls + '(\\s|$)');
    }

    return regClassCache[cls].test(ele[_getAttribute]('class') || '') && regClassCache[cls];
  };

  var addClass = function (ele, cls) {
    if (!hasClass(ele, cls)) {
      ele.setAttribute('class', (ele[_getAttribute]('class') || '').trim() + ' ' + cls);
    }
  };

  var removeClass = function (ele, cls) {
    var reg;

    if (reg = hasClass(ele, cls)) {
      ele.setAttribute('class', (ele[_getAttribute]('class') || '').replace(reg, ' '));
    }
  };

  var addRemoveLoadEvents = function (dom, fn, add) {
    var action = add ? _addEventListener : 'removeEventListener';

    if (add) {
      addRemoveLoadEvents(dom, fn);
    }

    loadEvents.forEach(function (evt) {
      dom[action](evt, fn);
    });
  };

  var triggerEvent = function (elem, name, detail, noBubbles, noCancelable) {
    var event = document.createEvent('Event');

    if (!detail) {
      detail = {};
    }

    detail.instance = lazysizes;
    event.initEvent(name, !noBubbles, !noCancelable);
    event.detail = detail;
    elem.dispatchEvent(event);
    return event;
  };

  var updatePolyfill = function (el, full) {
    var polyfill;

    if (!supportPicture && (polyfill = window.picturefill || lazySizesCfg.pf)) {
      if (full && full.src && !el[_getAttribute]('srcset')) {
        el.setAttribute('srcset', full.src);
      }

      polyfill({
        reevaluate: true,
        elements: [el]
      });
    } else if (full && full.src) {
      el.src = full.src;
    }
  };

  var getCSS = function (elem, style) {
    return (getComputedStyle(elem, null) || {})[style];
  };

  var getWidth = function (elem, parent, width) {
    width = width || elem.offsetWidth;

    while (width < lazySizesCfg.minSize && parent && !elem._lazysizesWidth) {
      width = parent.offsetWidth;
      parent = parent.parentNode;
    }

    return width;
  };

  var rAF = function () {
    var running, waiting;
    var firstFns = [];
    var secondFns = [];
    var fns = firstFns;

    var run = function () {
      var runFns = fns;
      fns = firstFns.length ? secondFns : firstFns;
      running = true;
      waiting = false;

      while (runFns.length) {
        runFns.shift()();
      }

      running = false;
    };

    var rafBatch = function (fn, queue) {
      if (running && !queue) {
        fn.apply(this, arguments);
      } else {
        fns.push(fn);

        if (!waiting) {
          waiting = true;
          (document.hidden ? setTimeout : requestAnimationFrame)(run);
        }
      }
    };

    rafBatch._lsFlush = run;
    return rafBatch;
  }();

  var rAFIt = function (fn, simple) {
    return simple ? function () {
      rAF(fn);
    } : function () {
      var that = this;
      var args = arguments;
      rAF(function () {
        fn.apply(that, args);
      });
    };
  };

  var throttle = function (fn) {
    var running;
    var lastTime = 0;
    var gDelay = lazySizesCfg.throttleDelay;
    var rICTimeout = lazySizesCfg.ricTimeout;

    var run = function () {
      running = false;
      lastTime = Date.now();
      fn();
    };

    var idleCallback = requestIdleCallback && rICTimeout > 49 ? function () {
      requestIdleCallback(run, {
        timeout: rICTimeout
      });

      if (rICTimeout !== lazySizesCfg.ricTimeout) {
        rICTimeout = lazySizesCfg.ricTimeout;
      }
    } : rAFIt(function () {
      setTimeout(run);
    }, true);
    return function (isPriority) {
      var delay;

      if (isPriority = isPriority === true) {
        rICTimeout = 33;
      }

      if (running) {
        return;
      }

      running = true;
      delay = gDelay - (Date.now() - lastTime);

      if (delay < 0) {
        delay = 0;
      }

      if (isPriority || delay < 9) {
        idleCallback();
      } else {
        setTimeout(idleCallback, delay);
      }
    };
  }; //based on http://modernjavascript.blogspot.de/2013/08/building-better-debounce.html


  var debounce = function (func) {
    var timeout, timestamp;
    var wait = 99;

    var run = function () {
      timeout = null;
      func();
    };

    var later = function () {
      var last = Date.now() - timestamp;

      if (last < wait) {
        setTimeout(later, wait - last);
      } else {
        (requestIdleCallback || run)(run);
      }
    };

    return function () {
      timestamp = Date.now();

      if (!timeout) {
        timeout = setTimeout(later, wait);
      }
    };
  };

  var loader = function () {
    var preloadElems, isCompleted, resetPreloadingTimer, loadMode, started;
    var eLvW, elvH, eLtop, eLleft, eLright, eLbottom, isBodyHidden;
    var regImg = /^img$/i;
    var regIframe = /^iframe$/i;
    var supportScroll = 'onscroll' in window && !/(gle|ing)bot/.test(navigator.userAgent);
    var shrinkExpand = 0;
    var currentExpand = 0;
    var isLoading = 0;
    var lowRuns = -1;

    var resetPreloading = function (e) {
      isLoading--;

      if (!e || isLoading < 0 || !e.target) {
        isLoading = 0;
      }
    };

    var isVisible = function (elem) {
      if (isBodyHidden == null) {
        isBodyHidden = getCSS(document.body, 'visibility') == 'hidden';
      }

      return isBodyHidden || !(getCSS(elem.parentNode, 'visibility') == 'hidden' && getCSS(elem, 'visibility') == 'hidden');
    };

    var isNestedVisible = function (elem, elemExpand) {
      var outerRect;
      var parent = elem;
      var visible = isVisible(elem);
      eLtop -= elemExpand;
      eLbottom += elemExpand;
      eLleft -= elemExpand;
      eLright += elemExpand;

      while (visible && (parent = parent.offsetParent) && parent != document.body && parent != docElem) {
        visible = (getCSS(parent, 'opacity') || 1) > 0;

        if (visible && getCSS(parent, 'overflow') != 'visible') {
          outerRect = parent.getBoundingClientRect();
          visible = eLright > outerRect.left && eLleft < outerRect.right && eLbottom > outerRect.top - 1 && eLtop < outerRect.bottom + 1;
        }
      }

      return visible;
    };

    var checkElements = function () {
      var eLlen, i, rect, autoLoadElem, loadedSomething, elemExpand, elemNegativeExpand, elemExpandVal, beforeExpandVal, defaultExpand, preloadExpand, hFac;
      var lazyloadElems = lazysizes.elements;

      if ((loadMode = lazySizesCfg.loadMode) && isLoading < 8 && (eLlen = lazyloadElems.length)) {
        i = 0;
        lowRuns++;

        for (; i < eLlen; i++) {
          if (!lazyloadElems[i] || lazyloadElems[i]._lazyRace) {
            continue;
          }

          if (!supportScroll || lazysizes.prematureUnveil && lazysizes.prematureUnveil(lazyloadElems[i])) {
            unveilElement(lazyloadElems[i]);
            continue;
          }

          if (!(elemExpandVal = lazyloadElems[i][_getAttribute]('data-expand')) || !(elemExpand = elemExpandVal * 1)) {
            elemExpand = currentExpand;
          }

          if (!defaultExpand) {
            defaultExpand = !lazySizesCfg.expand || lazySizesCfg.expand < 1 ? docElem.clientHeight > 500 && docElem.clientWidth > 500 ? 500 : 370 : lazySizesCfg.expand;
            lazysizes._defEx = defaultExpand;
            preloadExpand = defaultExpand * lazySizesCfg.expFactor;
            hFac = lazySizesCfg.hFac;
            isBodyHidden = null;

            if (currentExpand < preloadExpand && isLoading < 1 && lowRuns > 2 && loadMode > 2 && !document.hidden) {
              currentExpand = preloadExpand;
              lowRuns = 0;
            } else if (loadMode > 1 && lowRuns > 1 && isLoading < 6) {
              currentExpand = defaultExpand;
            } else {
              currentExpand = shrinkExpand;
            }
          }

          if (beforeExpandVal !== elemExpand) {
            eLvW = innerWidth + elemExpand * hFac;
            elvH = innerHeight + elemExpand;
            elemNegativeExpand = elemExpand * -1;
            beforeExpandVal = elemExpand;
          }

          rect = lazyloadElems[i].getBoundingClientRect();

          if ((eLbottom = rect.bottom) >= elemNegativeExpand && (eLtop = rect.top) <= elvH && (eLright = rect.right) >= elemNegativeExpand * hFac && (eLleft = rect.left) <= eLvW && (eLbottom || eLright || eLleft || eLtop) && (lazySizesCfg.loadHidden || isVisible(lazyloadElems[i])) && (isCompleted && isLoading < 3 && !elemExpandVal && (loadMode < 3 || lowRuns < 4) || isNestedVisible(lazyloadElems[i], elemExpand))) {
            unveilElement(lazyloadElems[i]);
            loadedSomething = true;

            if (isLoading > 9) {
              break;
            }
          } else if (!loadedSomething && isCompleted && !autoLoadElem && isLoading < 4 && lowRuns < 4 && loadMode > 2 && (preloadElems[0] || lazySizesCfg.preloadAfterLoad) && (preloadElems[0] || !elemExpandVal && (eLbottom || eLright || eLleft || eLtop || lazyloadElems[i][_getAttribute](lazySizesCfg.sizesAttr) != 'auto'))) {
            autoLoadElem = preloadElems[0] || lazyloadElems[i];
          }
        }

        if (autoLoadElem && !loadedSomething) {
          unveilElement(autoLoadElem);
        }
      }
    };

    var throttledCheckElements = throttle(checkElements);

    var switchLoadingClass = function (e) {
      var elem = e.target;

      if (elem._lazyCache) {
        delete elem._lazyCache;
        return;
      }

      resetPreloading(e);
      addClass(elem, lazySizesCfg.loadedClass);
      removeClass(elem, lazySizesCfg.loadingClass);
      addRemoveLoadEvents(elem, rafSwitchLoadingClass);
      triggerEvent(elem, 'lazyloaded');
    };

    var rafedSwitchLoadingClass = rAFIt(switchLoadingClass);

    var rafSwitchLoadingClass = function (e) {
      rafedSwitchLoadingClass({
        target: e.target
      });
    };

    var changeIframeSrc = function (elem, src) {
      try {
        elem.contentWindow.location.replace(src);
      } catch (e) {
        elem.src = src;
      }
    };

    var handleSources = function (source) {
      var customMedia;

      var sourceSrcset = source[_getAttribute](lazySizesCfg.srcsetAttr);

      if (customMedia = lazySizesCfg.customMedia[source[_getAttribute]('data-media') || source[_getAttribute]('media')]) {
        source.setAttribute('media', customMedia);
      }

      if (sourceSrcset) {
        source.setAttribute('srcset', sourceSrcset);
      }
    };

    var lazyUnveil = rAFIt(function (elem, detail, isAuto, sizes, isImg) {
      var src, srcset, parent, isPicture, event, firesLoad;

      if (!(event = triggerEvent(elem, 'lazybeforeunveil', detail)).defaultPrevented) {
        if (sizes) {
          if (isAuto) {
            addClass(elem, lazySizesCfg.autosizesClass);
          } else {
            elem.setAttribute('sizes', sizes);
          }
        }

        srcset = elem[_getAttribute](lazySizesCfg.srcsetAttr);
        src = elem[_getAttribute](lazySizesCfg.srcAttr);

        if (isImg) {
          parent = elem.parentNode;
          isPicture = parent && regPicture.test(parent.nodeName || '');
        }

        firesLoad = detail.firesLoad || 'src' in elem && (srcset || src || isPicture);
        event = {
          target: elem
        };
        addClass(elem, lazySizesCfg.loadingClass);

        if (firesLoad) {
          clearTimeout(resetPreloadingTimer);
          resetPreloadingTimer = setTimeout(resetPreloading, 2500);
          addRemoveLoadEvents(elem, rafSwitchLoadingClass, true);
        }

        if (isPicture) {
          forEach.call(parent.getElementsByTagName('source'), handleSources);
        }

        if (srcset) {
          elem.setAttribute('srcset', srcset);
        } else if (src && !isPicture) {
          if (regIframe.test(elem.nodeName)) {
            changeIframeSrc(elem, src);
          } else {
            elem.src = src;
          }
        }

        if (isImg && (srcset || isPicture)) {
          updatePolyfill(elem, {
            src: src
          });
        }
      }

      if (elem._lazyRace) {
        delete elem._lazyRace;
      }

      removeClass(elem, lazySizesCfg.lazyClass);
      rAF(function () {
        // Part of this can be removed as soon as this fix is older: https://bugs.chromium.org/p/chromium/issues/detail?id=7731 (2015)
        var isLoaded = elem.complete && elem.naturalWidth > 1;

        if (!firesLoad || isLoaded) {
          if (isLoaded) {
            addClass(elem, 'ls-is-cached');
          }

          switchLoadingClass(event);
          elem._lazyCache = true;
          setTimeout(function () {
            if ('_lazyCache' in elem) {
              delete elem._lazyCache;
            }
          }, 9);
        }

        if (elem.loading == 'lazy') {
          isLoading--;
        }
      }, true);
    });

    var unveilElement = function (elem) {
      if (elem._lazyRace) {
        return;
      }

      var detail;
      var isImg = regImg.test(elem.nodeName); //allow using sizes="auto", but don't use. it's invalid. Use data-sizes="auto" or a valid value for sizes instead (i.e.: sizes="80vw")

      var sizes = isImg && (elem[_getAttribute](lazySizesCfg.sizesAttr) || elem[_getAttribute]('sizes'));

      var isAuto = sizes == 'auto';

      if ((isAuto || !isCompleted) && isImg && (elem[_getAttribute]('src') || elem.srcset) && !elem.complete && !hasClass(elem, lazySizesCfg.errorClass) && hasClass(elem, lazySizesCfg.lazyClass)) {
        return;
      }

      detail = triggerEvent(elem, 'lazyunveilread').detail;

      if (isAuto) {
        autoSizer.updateElem(elem, true, elem.offsetWidth);
      }

      elem._lazyRace = true;
      isLoading++;
      lazyUnveil(elem, detail, isAuto, sizes, isImg);
    };

    var afterScroll = debounce(function () {
      lazySizesCfg.loadMode = 3;
      throttledCheckElements();
    });

    var altLoadmodeScrollListner = function () {
      if (lazySizesCfg.loadMode == 3) {
        lazySizesCfg.loadMode = 2;
      }

      afterScroll();
    };

    var onload = function () {
      if (isCompleted) {
        return;
      }

      if (Date.now() - started < 999) {
        setTimeout(onload, 999);
        return;
      }

      isCompleted = true;
      lazySizesCfg.loadMode = 3;
      throttledCheckElements();
      addEventListener('scroll', altLoadmodeScrollListner, true);
    };

    return {
      _: function () {
        started = Date.now();
        lazysizes.elements = document.getElementsByClassName(lazySizesCfg.lazyClass);
        preloadElems = document.getElementsByClassName(lazySizesCfg.lazyClass + ' ' + lazySizesCfg.preloadClass);
        addEventListener('scroll', throttledCheckElements, true);
        addEventListener('resize', throttledCheckElements, true);
        addEventListener('pageshow', function (e) {
          if (e.persisted) {
            var loadingElements = document.querySelectorAll('.' + lazySizesCfg.loadingClass);

            if (loadingElements.length && loadingElements.forEach) {
              requestAnimationFrame(function () {
                loadingElements.forEach(function (img) {
                  if (img.complete) {
                    unveilElement(img);
                  }
                });
              });
            }
          }
        });

        if (window.MutationObserver) {
          new MutationObserver(throttledCheckElements).observe(docElem, {
            childList: true,
            subtree: true,
            attributes: true
          });
        } else {
          docElem[_addEventListener]('DOMNodeInserted', throttledCheckElements, true);

          docElem[_addEventListener]('DOMAttrModified', throttledCheckElements, true);

          setInterval(throttledCheckElements, 999);
        }

        addEventListener('hashchange', throttledCheckElements, true); //, 'fullscreenchange'

        ['focus', 'mouseover', 'click', 'load', 'transitionend', 'animationend'].forEach(function (name) {
          document[_addEventListener](name, throttledCheckElements, true);
        });

        if (/d$|^c/.test(document.readyState)) {
          onload();
        } else {
          addEventListener('load', onload);

          document[_addEventListener]('DOMContentLoaded', throttledCheckElements);

          setTimeout(onload, 20000);
        }

        if (lazysizes.elements.length) {
          checkElements();

          rAF._lsFlush();
        } else {
          throttledCheckElements();
        }
      },
      checkElems: throttledCheckElements,
      unveil: unveilElement,
      _aLSL: altLoadmodeScrollListner
    };
  }();

  var autoSizer = function () {
    var autosizesElems;
    var sizeElement = rAFIt(function (elem, parent, event, width) {
      var sources, i, len;
      elem._lazysizesWidth = width;
      width += 'px';
      elem.setAttribute('sizes', width);

      if (regPicture.test(parent.nodeName || '')) {
        sources = parent.getElementsByTagName('source');

        for (i = 0, len = sources.length; i < len; i++) {
          sources[i].setAttribute('sizes', width);
        }
      }

      if (!event.detail.dataAttr) {
        updatePolyfill(elem, event.detail);
      }
    });

    var getSizeElement = function (elem, dataAttr, width) {
      var event;
      var parent = elem.parentNode;

      if (parent) {
        width = getWidth(elem, parent, width);
        event = triggerEvent(elem, 'lazybeforesizes', {
          width: width,
          dataAttr: !!dataAttr
        });

        if (!event.defaultPrevented) {
          width = event.detail.width;

          if (width && width !== elem._lazysizesWidth) {
            sizeElement(elem, parent, event, width);
          }
        }
      }
    };

    var updateElementsSizes = function () {
      var i;
      var len = autosizesElems.length;

      if (len) {
        i = 0;

        for (; i < len; i++) {
          getSizeElement(autosizesElems[i]);
        }
      }
    };

    var debouncedUpdateElementsSizes = debounce(updateElementsSizes);
    return {
      _: function () {
        autosizesElems = document.getElementsByClassName(lazySizesCfg.autosizesClass);
        addEventListener('resize', debouncedUpdateElementsSizes);
      },
      checkElems: debouncedUpdateElementsSizes,
      updateElem: getSizeElement
    };
  }();

  var init = function () {
    if (!init.i && document.getElementsByClassName) {
      init.i = true;

      autoSizer._();

      loader._();
    }
  };

  setTimeout(function () {
    if (lazySizesCfg.init) {
      init();
    }
  });
  lazysizes = {
    cfg: lazySizesCfg,
    autoSizer: autoSizer,
    loader: loader,
    init: init,
    uP: updatePolyfill,
    aC: addClass,
    rC: removeClass,
    hC: hasClass,
    fire: triggerEvent,
    gW: getWidth,
    rAF: rAF
  };
  return lazysizes;
});
let scrollpos = window.scrollY;
const header = document.getElementById('mainhead');
const vh90 = window.innerHeight * 0.9;

function convertRemToPixels(rem) {
  return rem * parseFloat(getComputedStyle(document.documentElement).fontSize);
}

function remove_heroclass_on_scroll() {
  header.classList.remove('herohead');
}

function add_heroclass_on_scroll() {
  header.classList.add('herohead');
}

function scrollDetect() {
  let lastScroll = 0;

  window.onscroll = function () {
    let currentScroll = document.documentElement.scrollTop || document.body.scrollTop;

    if (currentScroll > vh90 && lastScroll <= currentScroll) {
      lastScroll = currentScroll;
      header.classList.add('fade-out');
    } else {
      lastScroll = currentScroll;
      header.classList.remove('fade-out');
    }
  };
}

window.addEventListener('scroll', function () {
  scrollpos = window.scrollY;

  if (scrollpos > convertRemToPixels(1)) {
    remove_heroclass_on_scroll();
  } else {
    add_heroclass_on_scroll();
  }
});
scrollDetect();

function scrollRemoveOnMenu() {
  if (document.getElementById('mainnav-toggle-input').checked === true) {
    document.getElementById('mainhead').classList.add('nav-open');
    document.getElementById('mainnav-toggle').innerHTML = '⨯';
  } else {
    document.getElementById('mainhead').classList.remove('nav-open');
    document.getElementById('mainnav-toggle').innerHTML = '≡';
  }
}

document.getElementById('mainnav-toggle-input').addEventListener('click', scrollRemoveOnMenu);
let darkMode = localStorage.getItem('darkMode');
const darkModeToggle = document.querySelector('#dark-mode-toggle');

const enableDarkMode = () => {
  document.documentElement.classList.add('darkmode');
  localStorage.setItem('darkMode', 'enabled');
};

const disableDarkMode = () => {
  document.documentElement.classList.remove('darkmode');
  localStorage.setItem('darkMode', null);
};

if (darkMode === 'enabled') {
  enableDarkMode();
}

darkModeToggle.addEventListener('click', () => {
  darkMode = localStorage.getItem('darkMode');

  if (darkMode !== 'enabled') {
    enableDarkMode();
  } else {
    disableDarkMode();
  }
});