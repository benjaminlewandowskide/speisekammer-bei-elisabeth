---
title: Catering
#draft: true
menu: main
---

Für uns ist das, Ihnen jeden Essenswunsch von den Augen ablesen. Egal ob Geburtstag, Firmenevent, Hochzeit oder Familienfeier: Wir liefern Ihnen das passende Essen und den perfekten Service zu Ihnen nach Hause, in eine Eventlocation oder auch unter freiem Himmel!

Unser Ziel ist es, das Catering für sie zu einem Genuss werden zu lassen. Dabei unterstützen wir sie mit unserer jahrelangen Erfahrung und sorgen dafür, dass Ihre Veranstaltung kulinarisch gelingt. Damit sie Zeit haben sich auf Ihre Aufgabe als Gastgeber konzentrieren zu können.

## Wir bieten Ihnen

Warme oder kalte Buffets, Spezialitäten vom Grill oder aus der Pfanne und komplette Menüs sowie jede andere gewünschte Köstlichkeit bereiten wir liebend gerne für sie zu. Mit dem gewissen Extra an Liebe zum Detail damit sie Ihre Gäste kulinarisch komplett verwöhnen können. 

Selbstverständlich sind auch sowohl vegetarisch, vegane oder Wünsche bezüglich einer Nahrungsunverträglichkeit ein Teil unserer Menüplanung. Wir freuen uns bereits darauf, der Partner für Ihre Veranstaltung zu werden.
