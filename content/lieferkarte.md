---
title: Lieferkarte
layout: ordercard
menu: main
date: 2020-03-23T15:15:53Z

---
Sie können zwischen 12:00 Uhr und 21:00 Uhr telefonisch unter der Nummer [06174-24452](tel:0617424452 "06174-24452") bestellen.

Alle Preise verstehen sich inklusive der gesetzlichen Mehrwertsteuer von 7%.

**Auf Wunsch lege ich Ihnen gerne eine separate Speisekarte mit den kennzeichnungspflichtigen Inhaltsstoffen vor.**