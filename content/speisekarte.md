---
title: Speisekarte
layout: karte
draft: true
#menu:
#    main:
#        weight: 2
---

Alle Preise verstehen sich inklusive der gesetzlichen Mehrwertsteuer von 19 %.

**Auf Wunsch lege ich Ihnen gerne eine separate Speisekarte mit den kennzeichnungspflichtigen Inhaltsstoffen vor.**
