---
title: Stellenausschreibung
menu:
    main:
        weight: 3
---

Aktuell suchen wir einen ausgebildeten Koch in Festanstellung, inklusive Fahrkostenübernahme.

Es werden im Restaurant sowohl deutsche als auch polnische Gerichte serviert.

## Aufgaben:

-   Zeitnahe Vor- und Zubereitung aller Speisen gemäß deutscher und EU-Lebensmittelhygienevorschriften
-   Befolgung von Rezepten und Vorgaben zur Präsentation
-   Sichere und effiziente Bedienung von gängigen Küchengeräten
-   Säuberung und Instandhaltung des Arbeitsplatzes gemäß deutscher und EU Lebensmittelhygiene- und Sicherheitsvorschriften
-   Mithilfe beim Säubern und Aufräumen der Küche und aller Geräte
-   Aufstocken von Waren (je nach Bedarf, während der Schicht)
-   Befolgung aller Lebensmittelhygiene- und Sicherheitsvorschriften

## Zusätzliche Aufgaben:

-   Erfassung der Lebensmitteltemperatur
-   Erstellung von Berichten

Bitte bewerben Sie sich über das [Kontaktformular](/kontakt)
