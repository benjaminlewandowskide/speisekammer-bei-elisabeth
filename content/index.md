---
title: Willkommen
date: 2020-03-23T15:15:34Z

---
<h2 class="sr-only">Willkommen!</h2>

LIEBE GÄSTE,

ich freue mich darauf, für Sie zu kochen und Sie mit gutem Service zu verwöhnen.

**EIN HERZLICHES WILLKOMMEN!**

Meine aktuelle Frühlings-/Sommerkarte bietet bodenständige, rustikale Küche und zusätzlich täglich wechselnde Schmankerl für mittags und abends.

Jeden **Freitag** frischer Fisch – _nur so lange der Vorrat reicht_.

Ich verarbeite nur deutsches **Qualitätsfleisch** und frische **Produkte aus der Region**.

Für Ihre Feier stelle ich Ihnen mein Gasthaus gerne zur alleinigen Nutzung (ab 25 Personen) zur Verfügung. Sprechen Sie mich an, ich berate Sie gern.

Getreu meinem Motto: **reinkommen, wohlfühlen, wiederkommen**.

Ihre Gastgeberin

Elisabeth Kusnierz-Rutkowska